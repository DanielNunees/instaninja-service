<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->fingerprint();
});

Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');
Route::post('logout', 'AuthController@logout');
Route::post('refresh', 'AuthController@refresh');
Route::post('check', 'AuthController@me');
Route::post('sendCode', 'AuthController@sendCode');
//Route::get('me', 'AuthController@me');



Route::group(['middleware' => ['jwt.verify']], function () {

    Route::get('/test', 'TestController@test');
    //Hashtag Routes
    Route::post('/user/reference/hashtag/new', 'HashtagController@registerUserHashtag');
    //Location Routes
    Route::post('/searchLocation', 'LocationController@getLocationByName');
    Route::post('/user/reference/location/new', 'LocationController@registerReferenceLocation');
    Route::post('/user/reference/location/find', 'LocationController@getLocationByName');
    //Reference profile routes
    Route::post('/user/reference/profile/new', 'ReferenceProfileController@registerUserReferenceProfile');
    //Instagram Post Routes
    Route::post('/newTimelinePost', 'TimelineController@createNewPost');

    //People Routes
    Route::post('/tagged/people', 'PeopleController@taggedPeople');
    //Story Routes
    Route::post('/tagged/hashtag', 'HashtagController@taggedHashtag');
    //Analyzer Routes
    Route::get('/runHashtagAnalyzer', 'AnalyzerController@hashtagAnalyzer');
    Route::get('/hashtagStat', 'AnalyzerController@hashtagStat');

    Route::post('/media/new', 'MediaController@store');

    Route::post('/story/new', 'StoryController@store');
});
