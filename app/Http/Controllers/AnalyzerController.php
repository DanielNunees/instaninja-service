<?php

namespace App\Http\Controllers;

use App\Models\HashtagAnalyze;
use App\Models\People;
use App\Models\User;
use Illuminate\Support\Facades\Log;

class AnalyzerController extends Controller
{
    private $ig;
    private $people_controller;
    private $hashtag_controller;

    /**
     * LikesController constructor.
     * @param InstagramController $instagramController
     */
    public function __construct(InstagramController $instagramController)
    { }

    /**
     * @throws \Exception
     */
    public function hashtagAnalyzer()
    {

        $user = User::find(231949039);
        $instagramController = new InstagramController($user);
        $this->ig = $instagramController->getIg();
        $this->people_controller = new PeopleController($instagramController);
        $this->hashtag_controller = new HashtagController($instagramController);

        $references = UserController::getAllReferences($this->ig->account_id);
        fastcgi_finish_request();
        $rank_token = InstagramController::getRankToken();
        $maxId = null;
        do {
            try {
                $self_followers = $this->people_controller->getSelfFollowers($rank_token, null, $maxId);
                $users = $self_followers->getUsers();
                $count = count($users);
                foreach ($users as $follower) {
                    $count--;
                    $already_analyzed = HashtagAnalyzer::where('user_id', $follower->getPk())->first();
                    if (is_null($already_analyzed) && !$follower->getIsPrivate()) {
                        $person = People::findFollower($follower->getPk(), $references);
                        $hashtags = self::getFollowingHashtags($follower);
                        if (!is_null($hashtags) && count($hashtags) > 0) {
                            foreach ($hashtags as $hashtag) {
                                $reference_profile_user_id = null;
                                if (!is_null($person)) {
                                    $reference_profile_user_id = $person->reference_profile_user_id;
                                }
                                HashtagAnalyzer::updateOrCreate(
                                    ['user_id' => $follower->getPk(), 'hashtag' => $hashtag->getName()],
                                    ['reference_profile_user_id' => $reference_profile_user_id]
                                );
                            }
                        }
                        sleep(45);
                    }
                }
                sleep(120);
                $maxId = $self_followers->getNextMaxId();
                LogController::info($this->ig, "Analisador Fim da lista Get max ID: " . $maxId);
            } catch (\Exception $ex) {
                LogController::error($user, $ex->getMessage(), 'Register reference profile', $ex->getCode());
            }
        } while ($maxId !== null);
        LogController::info($this->ig, "Fim analyzer");
    }

    /**
     * @param $followers
     * @return mixed
     */
    public function getFollowingHashtags($followers)
    {
        try {
            return $this->hashtag_controller->getFollowingHashtags($followers->getPk());
        } catch (\Exception $ex) {
            return null;
        }
    }

    public function followersAnalyze()
    { //TODO

    }

    public function hashtagStat()
    {
        return HashtagAnalyzer::getHashtagCount();
    }
}
