<?php

namespace App\Http\Controllers;

use App\Jobs\LikeJob;
use App\Models\ActionSchedule;
use App\Models\UserConfiguration;
use App\Models\User;


class LikesController extends Controller
{
    private $user_configuration;

    /**
     * Calculate the time to start a new like job including the throttled_request_time if > 0
     */
    public function likeTime()
    {
        $like_time = now()->addMinutes(mt_rand($this->user_configuration->like_time_min, $this->user_configuration->like_time_max));
        $like_time = now()->createFromTimeString($like_time)->addMinutes($this->user_configuration->throttled_request_time);
        return $like_time;
    }

    public function startLikes()
    {
        $action_schedules = ActionSchedule::where('active', true)->where('like_time', '!=', null)->get();
        foreach ($action_schedules as $action_schedule) {
            $like_time = now()->createFromTimeString($action_schedule->like_time);
            $today = now();
            $user = User::find($action_schedule->user_id);
            $this->user_configuration = UserConfiguration::where('user_id', $user->user_id)->first();

            if ($like_time->lessThanOrEqualTo($today)) {
                self::dispatchLikeJob($user, $this->user_configuration);
                self::updateLikeTime($action_schedule->user_id);
            }
        }
    }

    /**
     * @param $user
     */
    public function dispatchLikeJob($user)
    {
        LikeJob::dispatch($user, $this->user_configuration)->onQueue('likes');
    }

    /**
     * Update the like_time to next request
     */
    public function updateLikeTime($user_id)
    {

        
        $action_schedule = ActionSchedule::where('user_id', $user_id)->first();
        $action_schedule->like_time = self::likeTime();
        $action_schedule->active = true;
        $action_schedule->save();
    }
}
