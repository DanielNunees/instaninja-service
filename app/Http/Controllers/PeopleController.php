<?php

namespace App\Http\Controllers;

use App\Jobs\DeleteDbFollowersJob;
use App\Models\People;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\TaggedPeople;

class PeopleController extends Controller
{
    private $ig;

    /**
     * @param InstagramController $instagramController
     */
    public function __construct(InstagramController $instagramController)
    {
        $this->ig = $instagramController->getIg();
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getInfoById($user_id)
    {
        try {
            $person = People::find($user_id);

            if (!is_null($person)) {
                return $person;
            } else {
                $user_info = $this->ig->people->getInfoById($user_id);
                $this->registerNewPerson($user_info->getUser(), null, "reference_profile");
                sleep(5);
                return $user_info->getUser();
            }
        } catch (\Exception $ex) {
            LogController::error($this->ig, $ex->getMessage(), 'Get Info By Id', $ex->getCode());
            throw $ex;
        }
    }




    /**
     * @param $person
     * @param $reference
     * @param $source
     * @return People
     */
    public static function registerNewPerson($person, $reference, $source)
    {

        try {
            $db_person = People::isNewPeople($person, $reference);
            if (is_null($db_person)) {
                $new_person = new People();

                switch ($source) {
                    case "reference_profile":
                        $new_person->reference_profile_user_id = $reference;
                        break;
                    case "hashtag":
                        $new_person->reference_hashtag_user_id = $reference;
                        break;
                    case "location":
                        $new_person->reference_location_user_id = $reference;
                        break;
                }

                $new_person->people_id = $person->getPk();
                $new_person->username = $person->getUsername();
                $new_person->full_name = $person->getFullName();
                $new_person->is_private = $person->getIsPrivate();
                $new_person->is_business = $person->getIsBusiness();
                $new_person->friendship_status = false;
                $new_person->is_active = false;
                $new_person->save();
                return $new_person;
            } else {
                return $db_person;
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
        return null;
    }

    /**
     * Get details about a specific user via their username.
     * Search first on the database
     * if don't find then use Instagram api
     */
    public function getInfoByName($username, $module = null)
    {
        try {

            $people = People::where('username', $username)->first();
            if (!is_null($people)) {
                return $people;
            }
            return $this->IGgetInfoByName($username, $module = null);
        } catch (\Exception $ex) {
            LogController::error($this->ig, $ex->getMessage(), 'Get Info By Name', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 200);
        }
    }


    /**
     * Get details about a specific user via their username.
     *
     * NOTE: The real app only uses this endpoint for profiles opened via "@mentions".
     *
     * @param string $username Username as string (NOT as a numerical ID).
     * @param string|null $module From which app module (page) you have opened the profile.
     *
     * @throws \InstagramAPI\Exception\InstagramException
     *
     * @return InstagramAPI\Response\Model\User
     *
     * @see People::getInfoById() For the list of supported modules.
     */
    public function IGgetInfoByName($username, $module = null)
    {
        try {
            $user_info = $this->ig->people->getInfoByName($username, $module);
            sleep(15);
            return $user_info->getUser();
        } catch (\Exception $ex) {
            LogController::error($this->ig, $ex->getMessage(), 'IG Get Info by name ', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 200);
        }
    }



    /**
     * Get followers from a User Reference Profile with filter
     *
     * @param $peopleRF
     * @param $user
     */
    public static function getFollowers($user, $peopleRF)
    {
        $instagram = new InstagramController($user);
        $ig = $instagram->getIg();
        try {

            $followers = $ig->people->getFollowers($peopleRF->people_id, $peopleRF->rank_token, null, $peopleRF->followers_max_id);
            sleep(35);
            LogController::info($ig, "Get new Followers by username(" . $peopleRF->username . ")");
            $users_pks = self::getUsersPks($followers->getUsers());
            $filtered_list = FilterController::filterUsers($followers->getUsers(), self::getFriendships($ig, $users_pks));


            //$final_list = FilterController::calculeSpamRatio(self::getUserInfo($ig, $filtered_list));
            self::registerPeopleList($filtered_list, $peopleRF->reference_profile_user_id, "reference_profile");

            $max_id = InstagramController::getNextMaxId($followers);
            $peopleRF->followers_max_id = $max_id;
            $peopleRF->save();
            sleep(35);
            return true;
        } catch (\Exception $ex) {
            LogController::error($user, $ex->getMessage(), 'Get followers', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 200);
        }
    }

    public static function getUsersPks($people)
    {
        $user_ids = array();
        foreach ($people as $person) {
            $user_ids[] = $person->getPk();
        }
        return $user_ids;
    }


    /**
     * Get info of a bulk of users
     * THIS WILL TAKE LONGER!!!
     */
    public static function getUserInfo($ig, $users)
    {
        $user_info_array = [];
        foreach ($users as $user) {
            try {
                sleep(mt_rand(60, 120));
                $user_info = $ig->people->getInfoById($user->getPk());
                $user_info_array[$user->getPk()] = $user_info->getUser();
                if (count($user_info_array) % 10 == 0)
                    sleep(600);
            } catch (\Exception $ex) {
                sleep(mt_rand(1000, 2000));
                LogController::error($ig, $ex->getMessage(), 'Get User Info', $ex->getCode());
                throw $ex;
            }
        }
        return $user_info_array;
    }




    /**
     * FriendshipsShowManyResponse.
     *
     * @method Model\UnpredictableKeys\FriendshipStatusUnpredictableContainer getFriendshipStatuses()
     * @method mixed getMessage()
     * @method string getStatus()
     * @method Model\_Message[] get_Messages()
     * @method bool isFriendshipStatuses()
     * @method bool isMessage()
     * @method bool isStatus()
     * @method bool is_Messages()
     *
     * @param $user_ids
     * @return array
     *
     * @throws \Exception
     */
    public static function getFriendships($ig, $user_ids)
    {
        try {
            $friendships = $ig->people->getFriendships($user_ids);
            $result = $friendships->getFriendshipStatuses();
            sleep(35);
            return $result->getData();
        } catch (\Exception $exception) {
            throw $exception;
        }
    }


    /**
     * @param $people
     * @param $reference_id
     * @param $source
     */
    public static function registerPeopleList($people, $reference_id, $source)
    {
        foreach ($people as $person) {
            self::registerNewPerson($person, $reference_id, $source);
        }
    }
    /**
     * Get details about a specific user via their username.
     *
     * NOTE: The real app only uses this endpoint for profiles opened via "@mentions".
     *
     * @param string $username Username as string (NOT as a numerical ID).
     * @param string|null $module From which app module (page) you have opened the profile.
     *
     * @throws \InstagramAPI\Exception\InstagramException
     *
     * @return InstagramAPI\Response\Model\User
     *
     * @see People::getInfoById() For the list of supported modules.
     */
    public static function getInfoByNameIG($username, $module = null)
    {
        try {
            $user = auth()->userOrFail();
            $people = People::where('username', $username)->first();
            if (!is_null($people)) {
                return $people;
            }
            $instagram = new InstagramController($user);
            $ig = $instagram->getIg();
            $user_info = $ig->people->getInfoByName($username, $module);
            $people = self::registerNewPerson($user_info->getUser(), null, null);
            sleep(20);
            return $people;
        } catch (\Exception $ex) {
            LogController::error($user, $ex->getMessage(), 'Get Info By Name IG ', $ex->getCode());
            return response()->json(["error" => $ex->getMessage()], 200);
        }
    }

    /**
     * @param Request $request
     * @throws \Exception
     */
    public static function taggedPeople($usernames)
    {
        try {
            $usernames = array_filter(json_decode($usernames));
            if (count($usernames) == 0) return response()->json(null, 200);
            $user = auth()->userOrFail();
            $tagged_people_id    = mt_rand(1, 10000000);
            foreach ($usernames as $key => $value) {
                sleep(10);
                $people = self::getInfoByNameIG($value);
                TaggedPeople::create(
                    [
                        'user_id' => $user->user_id,
                        'username' => $people->username,
                        'people_id' => $people->people_id,
                        'tagged_people_id' => $tagged_people_id
                    ]
                );
            }
            return response()->json($tagged_people_id, 200);
        } catch (\Exception $ex) {
            LogController::error($this->ig, $ex->getMessage(), 'People Tagged Service', $ex->getCode());
            throw new Exception($ex->getMessage(), 400);
        }
    }


    /**
     * FriendshipResponse.
     *
     * @method mixed getMessage()
     * @method string getStatus()
     * @method Model\_Message[] get_Messages()
     * @method bool isFriendshipStatuses()
     * @method bool isMessage()
     * @method bool isStatus()
     * @method bool is_Messages()
     *
     * @param $user_id
     * @return array
     *
     * @throws \Exception
     */
    public function getFriendship($user_id)
    {
        try {
            $friendship = $this->ig->people->getFriendship($user_id);
            sleep(65);
            return $friendship;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $rank_token
     * @param $searchQuery
     * @param $max_id
     * @return mixed
     * @throws \Exception
     */
    public function getSelfFollowers($rank_token, $searchQuery, $max_id)
    {
        try {
            return $this->ig->people->getSelfFollowers($rank_token, $searchQuery, $max_id);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     *
     */
    public function deleteDbFollowers()
    {
        try {
            DeleteDbFollowersJob::dispatch()->delay(now()->addSeconds(30));
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }
    }
}
