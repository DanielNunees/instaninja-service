<?php

namespace App\Http\Controllers;

use App\Models\PostMedia;
use App\Models\PostTimeline;
use Illuminate\Http\Request;
use Validator;

class TimelineController extends Controller
{

    private $ig;

    /**
     * TimelineController constructor.
     * @param InstagramController $instagramController
     */
    public function __construct()
    { }





    public static function getFeedMedia($user)
    {
        try {
            $instagram = new InstagramController($user);
            $ig = $instagram->getIg();
            $max_id = null;
            $filtered_media = [];
            $times = 0;
            $user_configuration = UserController::getUserConfiguration($user->user_id);
            $count = mt_rand($user_configuration->like_min, $user_configuration->like_max);
            do {
                $times++;
                $feed = self::getTimelineFeed($max_id, null, $ig);
                $filtered_media = array_merge($filtered_media, FilterController::filterFeedMedia($feed->getFeedItems()));

                if (count($filtered_media) >= $count) {
                    return array_slice($filtered_media, 0, $count);
                }
                $max_id = InstagramController::getNextMaxId($feed);
                sleep(65);
            } while (count($filtered_media) < $count && $max_id !== null && $times > 5);
            return $filtered_media;
        } catch (\Exception $ex) {
            LogController::error($user, $ex->getMessage(), 'Get feed Media', $ex->getCode());
            throw $ex;
        }
    }


    /**
     * User tag param to Timeline post only
     * @param $tagged_pattern
     * @return array
     */
    public static function postUserTags($post_user_tags)
    {
        $user_tags = [];
        $pos_x = 0.1;
        $pos_y = 0.1;
        foreach ($post_user_tags as $people) {
            $user_tags['in'][] = array('position' => [0.5 + $pos_x, 0.5 + $pos_y], 'user_id' => $people->people_id);
            $pos_x += 0.1;
            $pos_y += 0.1;
        }
        return $user_tags;
    }

    /**
     * Get your "home screen" timeline feed.
     *
     * This is the feed of recent timeline posts from people you follow.
     *
     * @param null|string $maxId Next "maximum ID", used for pagination.
     * @param null|array $options An associative array with following keys (all
     *                             of them are optional):
     *                             "latest_story_pk" The media ID in Instagram's
     *                             internal format (ie "3482384834_43294");
     *                             "seen_posts" One or more seen media IDs;
     *                             "unseen_posts" One or more unseen media IDs;
     *                             "is_pull_to_refresh" Whether this call was
     *                             triggered by a refresh;
     *                             "push_disabled" Whether user has disabled
     *                             PUSH;
     *                             "recovered_from_crash" Whether the app has
     *                             recovered from a crash/was killed by Android
     *                             memory manager/force closed by user/just
     *                             installed for the first time;
     *                             "feed_view_info" DON'T USE IT YET.
     *
     * @return \InstagramAPI\Response\TimelineFeedResponse|array
     *
     * @throws \Exception
     * @method bool getAutoLoadMoreEnabled()
     * @method bool getClientFeedChangelistApplied()
     * @method mixed getClientGapEnforcerMatrix()
     * @method string getClientSessionId()
     * @method Model\FeedItem[] getFeedItems()
     * @method string getFeedPillText()
     * @method bool getIsDirectV2Enabled()
     * @method Model\FeedAysf getMegaphone()
     * @method mixed getMessage()
     * @method bool getMoreAvailable()
     * @method string getNextMaxId()
     * @method int getNumResults()
     * @method mixed getPaginationInfo()
     * @method string getStatus()
     * @method string getViewStateVersion()
     * @method Model\_Message[] get_Messages()
     */
    public static function getTimelineFeed($maxId = null, $options = null, $ig)
    {
        try {
            $timeline = $ig->timeline->getTimelineFeed($maxId, $options);
            if (strpos($timeline->getStatus(), "ok") !== false) {
                return $timeline;
            } else {
                throw \Exception("Impossivel capturar o feed");
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
}
