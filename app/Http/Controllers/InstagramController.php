<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use InstagramAPI\Instagram;
use JWTAuth;
use PHPUnit\Runner\Exception;

class InstagramController extends Controller
{

    private $ig;

    /**
     * UserController constructor.
     * @param User $user
     * @throws \Exception
     */
    public function __construct(User $user)
    {
        Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;
        $this->ig = new Instagram(false, true);

        $this->getCredentials($user);
    }

    /**
     * @param User $user |null
     * @throws \Exception
     */
    public function getCredentials($user = null)
    {
        if (!is_null($user) && isset($user->username)) {
            $this->loginInstagram($user);
        } else {
            if ($user = auth()->userOrFail()) {
                $this->loginInstagram($user);
            }
        }
    }


    /**
     * @param User $user
     * @return void
     * @throws \Exception
     */
    public function loginInstagram(User $user)
    {
        try {
            $password = decrypt($user->ig_password);
            $loginResponse = $this->ig->login($user->username, $password);
        } catch (\Exception $ex) {
            LogController::error(null, $ex, $ex->getMessage(), $ex->getCode());
            throw $ex;
        }
    }


    public static function checkUserExistense($username, $password, $verification_code = null)
    {
        try {
            Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;
            $ig = new \InstagramAPI\Instagram(false, true);
            $loginResponse = $ig->login($username, $password);

            if ($loginResponse !== null && $loginResponse->isTwoFactorRequired() && $verification_code === null) {
                throw new Exception("Two Factor Required", 400);
            } else {
                if ($verification_code !== null)
                    self::twoFactorLogin($ig, $loginResponse, $username, $password, $verification_code);
            }
            return $ig->people->getInfoByName($username);
        } catch (\Exception $ex) {
            $response = $ex->getResponse();
            if (
                $ex instanceof \InstagramAPI\Exception\ChallengeRequiredException
                && $response->getErrorType() === 'checkpoint_challenge_required'
            ) {
                if ($verification_code !== null) {
                    self::sendCode($ig, $ex, $verification_code);
                }
                self::solveChallenges($ig, $ex);
            }
            throw new Exception($ex->getMessage(), 400);
        }
    }

    public static function twoFactorLogin($ig, $loginResponse, $username, $password, $verification_code)
    {

        if ($loginResponse !== null && $loginResponse->isTwoFactorRequired()) {
            try {
                $twoFactorIdentifier = $loginResponse->getTwoFactorInfo()->getTwoFactorIdentifier();
                LogController::error(null, 'Is two factor Required: Identifier - ' . $twoFactorIdentifier, '', 400);
                $ig->finishTwoFactorLogin($username, $password, $twoFactorIdentifier, $verification_code);
                return $ig->people->getInfoByName($username);
            } catch (\Exception $ex) {
                LogController::error(null, $ex->getMessage(), "Two factor authentication.", 109);
                throw new Exception("Something Went Wrong", 400);
            }
        } else {
            LogController::error(null, "Error Two factor authentication.", '', 110);
            throw new Exception("Something Went Wrong", 400);
        }
    }



    public static function solveChallenges($ig, \Exception $ex)
    {
        try {
            $response = $ex->getResponse();
            $checkApiPath = substr($response->getChallenge()->getApiPath(), 1);
            LogController::error(null, 'Solve Challenges', '', 107);
            $customResponse = $ig->request($checkApiPath)
                ->setNeedsAuth(false)
                ->addPost('choice', 1) //0 = SMS, 1 = Email
                ->addPost('_uuid', $ig->uuid)
                ->addPost('guid', $ig->uuid)
                ->addPost('device_id', $ig->device_id)
                ->addPost('_uid', $ig->account_id)
                ->addPost('_csrftoken', $ig->client->getToken())
                ->getDecodedResponse();
            sleep(20);
            throw new Exception("Two Factor is Required", 400);
        } catch (\Exception $ex) {
            LogController::error(null, $ex->getMessage(), '', 108);
            throw new Exception($ex->getMessage(), 400);
        }
    }

    public static function sendCode($ig, $ex, $verification_code)
    {
        try {
            LogController::error(null, "Sending Verification Code... " . $verification_code, '', 109);
            $response = $ex->getResponse();
            $checkApiPath = substr($response->getChallenge()->getApiPath(), 1);
            $customResponse = $ig->request($checkApiPath)
                ->setNeedsAuth(false)
                ->addPost('security_code', $verification_code)
                ->addPost('_uuid', $ig->uuid)
                ->addPost('guid', $ig->uuid)
                ->addPost('device_id', $ig->device_id)
                ->addPost('_uid', $ig->account_id)
                ->addPost('_csrftoken', $ig->client->getToken())
                ->getDecodedResponse();
            return $ig->people->getInfoById($ig->account_id);
        } catch (\Exception $ex) {
            LogController::error(null, $ex->getMessage(), "Challenge not solved", 120);
            throw new Exception($ex->getMessage(), 400);
        }
    }
    /**
     * @return mixed|string
     */
    public static function getRankToken()
    {
        $rankToken = \InstagramAPI\Signatures::generateUUID();
        return $rankToken;
    }

    /**
     * Max id, used to pagination
     *
     * @param $response
     * @return mixed
     */
    public static function getNextMaxId($response)
    {
        $maxId = $response->getNextMaxId();
        return $maxId;
    }

    /**
     * @return mixed
     */
    public function getIg()
    {
        return $this->ig;
    }
}
