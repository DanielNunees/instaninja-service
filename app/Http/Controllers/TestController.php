<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserConfiguration;
use App\Models\People;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\User;
use App\Models\ActionSchedule;
use InstagramAPI\Instagram;
use InstagramAPI\Exception\ChallengeRequiredException;
use App\Models\ReferenceProfile;
use App\Models\PostStory;

class TestController extends InstagramController
{

    private $ig;
    private $user;
    private $user_configuration;
    public function __construct(InstagramController $instagramController)
    {
        $this->user = auth()->userOrFail();
        // $this->location_controller = new LocationController($instagramController);
        $this->user_configuration = UserConfiguration::where('user_id', $this->user->user_id)->first();
        $this->ig = $instagramController->getIg();
    }

    /**
     * Ger a random reference profile
     * @return int
     */
    public static function getRandom($user)
    {
        return ReferenceProfile::getRandomProfile($user->user_id);
    }

    /**
     * @param Request $request
     * @throws \Exception
     */
    public function test()
    {

    }
}





















// try {
//     $timeline_posts = PostStory::getPosts();
//     foreach ($timeline_posts as $post) {
//         $user = User::find($post->user_id);
//         return $post->tagged_people_id;
//         //$post->tagged_users = 
//         return StoryController::storyMentions($post->tagged_people_id);
//         return $post;


//         //$post->PostTimelinePhotoJob::dispatch($user, $post); //->onQueue('posts');
//     }
// } catch (\Exception $ex) {
//     LogController::error(User::find($post->user_id), $ex->getMessage(), "Timeline post Controller", $ex->getCode());
// }
