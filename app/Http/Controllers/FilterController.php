<?php

namespace App\Http\Controllers;

class FilterController extends Controller
{
    /**
     * @param $media_list
     * @return array
     */
    public static function filterFeedMedia($media_list)
    {
        $filtered_list = [];
        foreach ($media_list as $item) {
            if (
                !is_null($item)
                && !is_null($item->getMediaOrAd())
                && !$item->getMediaOrAd()->getHasLiked()
                && array_search($item, $filtered_list) === false
                && !is_null($item->getMediaOrAd()->getUser())
                && is_null($item->getMediaOrAd()->getInjected())
                && self::filterWords($item->getMediaOrAd()->getUser()->getUsername())
            ) {
                $filtered_list[] = $item;
            }
        }
        return $filtered_list;
    }

    /**
     * @param $username
     * @return bool
     */
    public static function filterWords($username)
    {
        $words = [
            'gay', 'men', 'sex', 'cueca', 'sunga', 'boy', 'tvermodelos1',
            '1807_antonie', 'shop', 'buy', 'price', 'shop', 'loja', 'preço',
            'nails', 'peludo', 'barbado', 'loirosperfeitos', 'jesus', 'biblia',
            'deus', 'cristo', 'unha', 'moda'
        ];
        foreach ($words as $word) {
            if (strpos($username, $word) !== false) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $media_list
     * @return array
     */
    public static function filterLocationMedia($media_list)
    {
        $filtered_list = [];
        foreach ($media_list as $item) {
            if (
                !is_null($item)
                && array_search($item, $filtered_list) == false
                && !$item->getHasLiked()
                && !is_null($item->getUser())
                && !is_null($item->getUser()->getUsername())
                && self::filterWords($item->getUser()->getUsername())
                && !is_null($item->getUser()->getFullName())
                && self::filterWords($item->getUser()->getFullName())
            ) {
                $filtered_list[] = $item;
            }
        }
        return $filtered_list;
    }

    /**
     * Filter media
     *
     * if is null
     * if i've liked
     * if username contains blocked words
     * @param $media_list
     * @return array
     */
    public static function filterHashtagMedia($media_list)
    {
        $filtered_list = [];
        foreach ($media_list as $item) {
            if (
                !is_null($item)
                && array_search($item, $filtered_list) == false
                && !$item->getHasLiked()
                && !is_null($item->getUser())
                && !is_null($item->getUser()->getUsername())
                && self::filterWords($item->getUser()->getUsername())
                && !is_null($item->getUser()->getFullName())
                && self::filterWords($item->getUser()->getFullName())
            ) {
                $filtered_list[] = $item;
            }
        }
        return $filtered_list;
    }

    /**
     * 
     * In cases like getUser from hashtags or locations they alredy come 
     * with the information about friendship, that's why the second param
     * could be empty
     * 
     * Filter users, by rules
     * 1. No profile pic
     * 2. No posted media
     * 3. Private account
     * 4. User name in block wordlist
     * 5. Full name in block wordlist
     * 6. If i already follow them
     * 7. TODO-> followers/following < fator
     *
     * @param $users
     * @param array $friendship_status
     * @return array
     */
    public static function filterUsers($users, $friendship_status = [])
    {
        $filtered_list = [];
        $to_follow = [];

        foreach ($users as $user) {
            if (
                !is_null($user) //First case, when the friendship status do not come into request
                && array_search($user, $filtered_list) == false
                && !$user->getIsPrivate()
                && !is_null($user->getUsername())
                && self::filterWords($user->getUsername())
                && !is_null($user->getFullName())
                && self::filterWords($user->getFullName())
            ) {
                $filtered_list[$user->getPk()] = $user;
                if (!is_null($user->getFriendshipStatus())) {
                    $person = $user->getFriendshipStatus();
                    if (
                        !$person->getFollowing()
                        && !$person->getOutgoingRequest()
                        && !in_array($user, $to_follow)
                    ) {
                        $to_follow[] = $user;
                    }
                }
            }
        }

        foreach ($friendship_status as $id => $person) {
            if (
                !$person->getFollowing()
                && !$person->getOutgoingRequest()
                && array_key_exists($id, $filtered_list)
            ) {
                $to_follow[] = $filtered_list[$id];
            }
        }
        return $to_follow;
    }


    public static function calculeSpamRatio($user_info)
    {
        $user_list = [];
        foreach ($user_info as $user) {
            $ratio = ($user->getFollowerCount() / $user->getFollowingCount());
            if ($ratio >= 1) {
                $user_list[] = $user;
            }
        }
        return $user_list;
    }
}
