<?php

namespace App\Http\Controllers;

use App\Jobs\FollowPeopleJob;
use App\Jobs\UnfollowPeopleJob;
use App\Models\ActionSchedule;
use App\Models\UserConfiguration;
use App\Models\User;
use App\Models\People;

class FollowersController extends Controller
{

    private $user_configuration;

    public function startFollow()
    {
        $action_schedules = ActionSchedule::where('active', true)->where('follow_time', '!=', null)->get();
        foreach ($action_schedules as $action_schedule) {
            $follow_time = now()->createFromTimeString($action_schedule->follow_time);
            $unfollow_time = now()->createFromTimeString($action_schedule->unfollow_time);
            $today = now();
            $user = User::find($action_schedule->user_id);
            $user_stats = UserStatsController::getLastUserStat($user->user_id);
            $this->user_configuration = UserConfiguration::where('user_id', $user->user_id)->first();
            self::getMoreFollowers($user);

            if ($follow_time->lessThanOrEqualTo($today)) {
                if ($user_stats->following_count < $this->user_configuration->max_following) {
                    self::dispatchFollowJob($user);
                }
                self::updateFollowTime($action_schedule->user_id);
            }
            if ($unfollow_time->lessThanOrEqualTo($today)) {
                self::dispatchUnfollowJob($user);
                self::updateUnfollowTime($action_schedule->user_id);
            }
        }
    }
    /**
     * Update the time_to_follow to next request
     */
    public function updateFollowTime($user_id)
    {
        $action_schedule = ActionSchedule::where('user_id', $user_id)->first();
        $action_schedule->follow_time = self::followTime();
        $action_schedule->active = true;
        $action_schedule->save();
    }
    /**
     * Update the time_to_unfollow to next request
     */
    public function updateUnfollowTime($user_id)
    {
        $action_schedule = ActionSchedule::where('user_id', $user_id)->first();
        $action_schedule->unfollow_time = self::unfollowTime();
        $action_schedule->active = true;
        $action_schedule->save();
    }

    /**
     * Calculate the time to start a new follow job including the throttled_request_time if > 0
     */
    public function followTime()
    {
        $follow_time = now()->addMinutes(mt_rand($this->user_configuration->follow_time_min, $this->user_configuration->follow_time_max));
        $follow_time = now()->createFromTimeString($follow_time)->addMinutes($this->user_configuration->throttled_request_time);
        return $follow_time;
    }

    /**
     * Calculate the time to start a new unfollow job including the throttled_request_time if > 0
     */
    public function unfollowTime()
    {
        $unfollow_time = now()->addMinutes(mt_rand($this->user_configuration->unfollow_time_min, $this->user_configuration->unfollow_time_max));
        $unfollow_time = now()->createFromTimeString($unfollow_time)->addMinutes($this->user_configuration->throttled_request_time);
        return $unfollow_time;
    }

    /**
     * Dispatch a new follow job to a default queue with the user configurations times and values
     */
    public function dispatchFollowJob($user)
    {
        FollowPeopleJob::dispatch($user, $this->user_configuration);
    }


    /**
     *Dispatch a new unfollow job to a default queue with the user configurations times and values
     */
    public function dispatchUnfollowJob($user)
    {
        UnfollowPeopleJob::dispatch($user, $this->user_configuration);
    }

    /**
     * @param $rankToken_reference_profiles
     * @param $rankToken_reference_hashtag
     * @param $rank_token_reference_location
     * @throws \Exception
     */
    public function getMoreFollowers($user)
    {
        $aux = UserController::getAllReferences($user->user_id);
        $peopleRF = People::getAllReferenceFollowers($aux, false, false);
        $action_schedule = ActionSchedule::where('user_id', $user->user_id)->first();
        if ($peopleRF->count() < 5 && !($action_schedule->getting_followers)) {
            $action_schedule->getting_followers = true;
            $action_schedule->save();
            ReferenceProfileController::getMoreFollowers($user);
            $variable =  mt_rand(1, 2);
            switch ($variable) {
                case "1":
                    HashtagController::getMoreFollowers($user);
                    break;
                case "2":
                    LocationController::getMoreFollowers($user);
                    break;
            }
            $action_schedule->getting_followers = false;
            $action_schedule->save();
        }
    }
}
