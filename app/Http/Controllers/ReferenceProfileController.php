<?php

namespace App\Http\Controllers;

use App\Models\ReferenceProfile;
use Illuminate\Http\Request;
use Validator;

class ReferenceProfileController extends Controller
{
    private $ig;
    private $people_controller;

    /**
     * ReferenceProfileController constructor.
     * @throws \Exception
     */
    public function __construct(InstagramController $instagramController)
    {
        $this->ig = $instagramController->getIg();
        $this->people_controller = new PeopleController($instagramController);
    }

    /**
     * @param $reference_profile_user_id
     */
    public static function incrementFollowers($reference_profile_user_id)
    {
        ReferenceProfile::incrementFollowers($reference_profile_user_id);
    }


    /**
     * * @param User
     * @return int
     */
    public static function getMoreFollowers($user)
    {
        $reference_profile = self::getRandom($user);
        if (isset($reference_profile)) {
            if (is_null($reference_profile->rank_token)) {
                $reference_profile->rank_token = InstagramController::getRankToken();
                $reference_profile->save();
            }
            PeopleController::getFollowers($user, $reference_profile);
        }
        return true;
    }

    /**
     * Ger a random reference profile
     * @return int
     */
    public static function getRandom($user)
    {
        return ReferenceProfile::getRandomProfile($user->user_id);
    }


    public function registerUserReferenceProfile(Request $request)
    {
        try {
            $user = auth()->userOrFail();
            $username = $this->validator($request);
            if (ReferenceProfile::checkReferenceProfile($user, $username)->isEmpty()) {
                return $this->newReferenceProfile($username);
            } else {
                return response()->json(["info" => "Profile is already registered on system"], 200);
            }
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Register reference profile', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 200);
        }
    }

    /**
     * @param Request $request
     * @return array|null|string
     * @throws \Exception
     */
    public function validator(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'reference_profile' => 'required',
        ]);
        if ($validator->fails()) {
            throw new \Exception($validator->errors(), 500);
        } else {
            return $request->input('reference_profile');
        }
    }

    /**
     * @param $reference_people_usernames
     * @return array
     */
    public function newReferenceProfile($username)
    {
        try {
            $user = auth()->userOrFail();
            $person = $this->people_controller->IGgetInfoByName($username);
            if ($person->getIsPrivate()) {
                return response()->json(["error" => "Reference profile is private"], 200);
            }
            $reference_profile = new ReferenceProfile();
            $reference_profile->user_id = $user->user_id;
            $reference_profile->people_id = $person->getPk();
            $reference_profile->username = $person->getUsername();
            $reference_profile->reference_profile_user_id = base64_encode($this->ig->account_id . $person->getPk());
            $reference_profile->profile_pic_url = $person->getProfilePicUrl();
            $reference_profile->is_active = true;
            $reference_profile->save();
            return response()->json(["success" => "Reference profile registered"], 200);
        } catch (\Excepiton $ex) {
            throw $ex;
        }
    }
}
