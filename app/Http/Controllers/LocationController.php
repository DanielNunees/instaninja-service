<?php

namespace App\Http\Controllers;

use App\Models\ReferenceLocations;
use Exception;
use Illuminate\Http\Request;
use Validator;

class LocationController extends Controller
{
    private $ig;
    private $user_configuration;
    private $people_controller;

    /**
     * @param InstagramController $instagramController
     */
    public function __construct(InstagramController $instagramController)
    {
        $this->ig = $instagramController->getIg();
        $this->user_configuration = UserController::getUserConfiguration($this->ig->account_id);
        $this->people_controller = new PeopleController($instagramController);
    }

    /**
     * @param $reference_location_user_id
     */
    public static function incrementFollowers($reference_location_user_id)
    {
        ReferenceLocations::incrementFollowers($reference_location_user_id);
    }

    /**
     * Search for Facebook locations by name.
     *
     * WARNING: The locations found by this function DO NOT work for attaching
     * locations to media uploads. Use Location::search() instead!
     *
     * @param string         $query       Finds locations containing this string.
     * @param string[]|int[] $excludeList Array of numerical location IDs (ie "17841562498105353")
     *                                    to exclude from the response, allowing you to skip locations
     *                                    from a previous call to get more results.
     * @param string|null    $rankToken   (When paginating) The rank token from the previous page's response.
     *
     * @throws \InvalidArgumentException
     * @throws \InstagramAPI\Exception\InstagramException
     *
     * @return \InstagramAPI\Response\FBLocationResponse
     *
     * @see FBLocationResponse::getRankToken() To get a rank token from the response.
     * @see examples/paginateWithExclusion.php For an example.
     */
    public function getLocationByName(Request $request)
    {
        try {
            $location = $this->validator($request);
            $location_list = $this->ig->location->findPlaces($location, $excludeList = [], $rankToken = null)->getItems();
            return response()->json($location_list, 200);
        } catch (Exception $ex) {
            return response()->json($ex->getMessage(), 500);
        }
    }




    /**
     * @param Request $request
     * @return Exception|\Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function registerReferenceLocation(Request $request)
    {
        try {
            $user = auth()->userOrFail();
            $location = $this->validator($request);
            $lat = $location['lat'];
            $lng = $location['lng'];
            $venues = $this->ig->location->search($lat, $lng)->getVenues()[0];
            if (ReferenceLocations::checkReferenceLocation($user, $venues->getAddress())->isEmpty()) {
                return $this->newReferenceLocation($venues);
            } else {
                return response()->json(["info" => "Location is already registered on system"], 200);
            }
        } catch (Exception $ex) {
            throw $ex;
        }
    }


    /**
     * @param $reference_people_usernames
     * @return array
     */
    public function newReferenceLocation($location)
    {
        try {
            $user = auth()->userOrFail();
            $reference_location = new ReferenceLocations();
            $reference_location->user_id = $user->user_id;
            $reference_location->external_id = $location->getExternalId();
            $reference_location->address = $location->getAddress();
            $reference_location->name = $location->getName();
            $reference_location->latitude = $location->getLat();
            $reference_location->longitude = $location->getLng();
            $reference_location->rank_token = InstagramController::getRankToken();
            $reference_location->reference_location_user_id = base64_encode($this->ig->account_id . $location->getAddress());
            $reference_location->is_active = true;
            $reference_location->save();
            return response()->json(["success" => "Reference Location registered"], 200);
        } catch (\Excepiton $ex) {
            throw $ex;
        }
    }



    /**
     * @param Request $request
     * @return array|null|string
     * @throws Exception
     */
    public function validator($request)
    {
        $validator = Validator::make($request->all(), [
            'reference_location' => 'required',
        ]);
        if ($validator->fails()) {
            throw new Exception($validator->errors());
        } else {
            return $request->input('reference_location');
        }
    }
    /**
     * @param $location
     * @param $rank_token
     * @return array|null
     * @throws \Exception
     */
    public function getLocationMedia($location, $user)
    {
        try {
            $filtered_media = [];
            $this->user_configuration = UserController::getUserConfiguration($this->ig->account_id);
            $count = mt_rand($this->user_configuration->like_min, $this->user_configuration->like_max);
            $max_id = null;
            do {
                $feed = self::getLocationFeed($location, $user, null);
                if (!is_null($feed)) {
                    $feed = $feed->getSections();
                    foreach ($feed as $aux) {
                        $layout_content = $aux->getLayoutContent();
                        $section_medias[] = $layout_content->getMedias();
                    }
                    foreach ($section_medias as $section_media) {
                        $media[] = $section_media[0]->getMedia();
                    }
                    $filtered_media = array_merge($filtered_media, FilterController::filterLocationMedia($media));

                    if (count($filtered_media) >= $count) {
                        return array_slice($filtered_media, 0, $count);
                    } else {
                        $filtered_media = array_merge($filtered_media, FilterController::filterLocationMedia($feed->getRankedItems()));
                    }
                    $max_id = InstagramController::getNextMaxId($feed);
                }
                sleep(65);
            } while ($max_id !== null);
            return null;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * @param $location
     * @param $rank_token
     * @return void
     * @throws \Exception
     */
    public static function getLocationUsers($location, $user)
    {
        try {
            $users = [];
            LogController::info($user, "Get new users by location: " . $location->address);
            $feed = self::getLocationFeed($location, $user, null);
            $location->location_max_id = InstagramController::getNextMaxId($feed);
            $location->save();
            $feed = $feed->getSections();
            foreach ($feed as $aux) {
                $layout_content = $aux->getLayoutContent();
                $section_medias[] = $layout_content->getMedias();
            }
            foreach ($section_medias as $section_media) {
                $media = $section_media[0]->getMedia();
                $users[] = $media->getUser();
            }
            $filtered_list = FilterController::filterUsers($users, []);
            if (count($filtered_list) > 5)
                PeopleController::registerPeopleList($filtered_list, $location->reference_location_user_id, "location");
            sleep(65);
        } catch (\Exception $ex) {
            LogController::error($user, $ex, 'Get Location Users', $ex->getCode());
        }
    }

    /**
     * Ger a random User Reference Location from database with user_id
     * @param int $quantity
     * @return mixed
     * @throws \Exception
     */
    public static function getRandom($user)
    {
        try {
            return ReferenceLocations::getRandomLocation($user->user_id);
        } catch (\Exception $ex) {
            LogController::error($user, $ex->getMessage(), 'Get Random Location', $ex->getCode());
            throw $ex;
        }
    }

    /**
     * @param $location
     * @param $rankToken
     * @param null $maxId
     * @return
     * @throws \Exception
     */
    public static function getLocationFeed($location, $user, $maxId)
    {
        try {
            $instagram = new InstagramController($user);
            $ig = $instagram->getIg();
            $location_id = $location['external_id'];
            return $ig->location->getFeed(
                $location_id,
                $location->rank_token,
                $tab = 'ranked',
                $nextMediaIds = null,
                $nextPage = null,
                $maxId
            );
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Get Follwers from one of the reference locations
     * @param $user
     * @throws \Exception
     */
    public static function getMoreFollowers($user)
    {
        $reference_location = self::getRandom($user);
        if (!is_null($reference_location)) {
            self::getLocationUsers($reference_location, $user);
        }
    }
}
