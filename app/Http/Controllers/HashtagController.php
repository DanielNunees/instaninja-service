<?php

namespace App\Http\Controllers;

use App\Models\ReferenceHashtag;
use Exception;
use JWTAuth;
use Validator;
use Illuminate\Http\Request;
use App\Models\TaggedHashtag;

class HashtagController extends Controller
{

    private $ig;
    private $people_controller;

    /**
     * LikesController constructor.
     * @param InstagramController $instagramController
     */
    public function __construct(InstagramController $instagramController)
    {
        $this->ig = $instagramController->getIg();
        $this->people_controller = new PeopleController($instagramController);
    }

    /**
     * @param $reference_hashtag_user_id
     */
    public static function incrementFollowers($reference_hashtag_user_id)
    {
        ReferenceHashtag::incrementFollowers($reference_hashtag_user_id);
    }

    /**
     * @param User
     * @throws Exception
     */
    public static function getMoreFollowers($user)
    {
        try {
            $reference_hashtag = self::getRandom($user);
            if (!is_null($reference_hashtag)) {
                $reference_hashtag->rank_token = InstagramController::getRankToken();
                $reference_hashtag->save();
                self::getHashtagUsers($reference_hashtag, $user, null);
            }
        } catch (\Exception $ex) {
            LogController::error($user, $ex->getMessage(), 'Get More Followers from hashtag', $ex->getCode());
        }
    }


    /**
     * @param Request $request
     * @throws \Exception
     */
    public static function taggedHashtag($hashtags)
    {
        try {
            $hashtags = array_filter(json_decode($hashtags));
            if (count($hashtags) == 0) return response()->json(null, 200);
            $user = auth()->userOrFail();
            $tagged_hashtag_id = mt_rand(1, 10000000);
            foreach ($hashtags as $value) {
                sleep(10);
                TaggedHashtag::create(
                    [
                        'user_id' => $user->user_id,
                        'hashtag' => $value,
                        'tagged_hashtag_id' => $tagged_hashtag_id
                    ]
                );
            }
            return response()->json($tagged_hashtag_id, 200);
        } catch (\Exception $ex) {
            LogController::error($this->ig, $ex->getMessage(), 'Tagged Hashtag Service', $ex->getCode());
            throw new Exception($ex->getMessage(), 400);
        }
    }



    /**
     * Ger a random reference hashtag from database with user_id
     * @param int $quantity
     * @return mixed
     * @throws Exception
     */
    public static function getRandom($user)
    {
        try {
            return ReferenceHashtag::getRandomHashtag($user->user_id);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * @param $hashtag
     * @param $rank_token
     * @return void
     * @throws Exception
     */
    public static function getHashtagUsers($hashtag, $user)
    {
        try {
            $users = [];
            LogController::info($user, "Get new users by hashtag: " . $hashtag->hashtag);
            $feed = self::getHashtagFeed($hashtag, $user, null);
            $hashtag->hashtag_max_id = InstagramController::getNextMaxId($feed);
            $hashtag->save();
            $feed = $feed->getItems();
            foreach ($feed as $user) {
                $users[] = $user->getUser();
            }
            $filtered_list = FilterController::filterUsers($users, []);
            if (count($filtered_list) > 10)
                PeopleController::registerPeopleList($filtered_list, $hashtag->reference_hashtag_user_id, "hashtag");
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * @param string $hashtag
     * @param string $rank_token
     * @param string|null $max_id
     * @return mixed
     */
    public static function getHashtagFeed($hashtag, $user, $max_id = null)
    {
        try {
            $instagram = new InstagramController($user);
            $ig = $instagram->getIg();
            return $ig->hashtag->getFeed($hashtag->hashtag, $hashtag->rank_token, $max_id);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * @param Request $request
     * @return Exception|\Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function registerUserHashtag(Request $request)
    {
        try {
            $user = auth()->userOrFail();
            $hashtag = $this->validator($request);
            if (ReferenceHashtag::checkReferenceHashtag($user, $hashtag)->isEmpty()) {
                return $this->newReferenceHashtag($hashtag);
            } else {
                return response()->json(["info" => "Profile is already registered on system"], 200);
            }
            return response()->json(["info" => "Hashtag Reference Created"], 200);
        } catch (Exception $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Register reference profile', $ex->getCode());
            throw $ex;
        }
    }

    /**
     * @param $reference_hahstag
     * @return array
     */
    public function newReferenceHashtag($hashtag)
    {
        try {
            $user = auth()->userOrFail();
            $reference_hashtag = new ReferenceHashtag();
            $reference_hashtag->user_id = $user->user_id;
            $reference_hashtag->hashtag = $hashtag;
            $reference_hashtag->rank_token = InstagramController::getRankToken();
            $reference_hashtag->reference_hashtag_user_id = base64_encode($this->ig->account_id . $hashtag);
            $reference_hashtag->is_active = true;
            $reference_hashtag->save();
            return response()->json(["success" => "Reference profile registered"], 200);
        } catch (\Excepiton $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Register reference profile', $ex->getCode());
            throw $ex;
        }
    }

    /**
     * @param Request $request
     * @return array|null|string
     * @throws Exception
     */
    public function validator(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'reference_hashtag' => 'required',
        ]);
        if ($validator->fails()) {
            throw new Exception($validator->errors());
        } else {
            return $request->input('reference_hashtag');
        }
    }





    /**
     * Get the hashtags that the user follow on instagram
     * @param $user_id
     * @return
     * @throws Exception
     */
    public function getFollowingHashtags($user_id)
    {
        try {
            return $this->ig->hashtag->getFollowing($user_id)->getTags();
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * @param $hashtag
     * @return array|null
     * @throws Exception
     */
    public function getHashtagMedia($hashtag)
    {
        try {
            $filtered_media = [];
            $user_configuration = UserController::getUserConfiguration($this->ig->account_id);
            $count = mt_rand($user_configuration->like_min, $user_configuration->like_max);
            $max_id = null;
            do {
                $feed = self::getHashtagFeed($hashtag->hashtag, $hashtag->rank_token, $max_id);
                if (!is_null($feed)) {
                    $filtered_media = array_merge($filtered_media, FilterController::filterHashtagMedia($feed->getItems()));
                    if (count($filtered_media) >= $count) {
                        return array_slice($filtered_media, 0, $count);
                    }
                    $max_id = InstagramController::getNextMaxId($feed);
                    sleep(65);
                }
            } while (count($filtered_media) < $count && $max_id !== null);
            return null;
        } catch (Exception $ex) {
            throw $ex;
        }
    }
}
