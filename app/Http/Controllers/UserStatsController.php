<?php

namespace App\Http\Controllers;

use App\Models\LikesStats;
use App\Models\People;
use App\Models\ReferenceHashtag;
use App\Models\ReferenceLocations;
use App\Models\ReferenceProfile;
use App\Models\User;
use App\Models\UserStats;
use JWTAuth;

class UserStatsController extends Controller
{

    private $ig;

    /**
     *
     * @throws \Exception
     */
    public static function updateHourlyStats()
    {
        $users = User::all();

        foreach ($users as $user) {
            try {
                if ($user->is_active) {
                    $instagramController = new InstagramController($user);
                    $ig = $instagramController->getIg();
                    $self_info = $ig->people->getSelfInfo()->getUser();
                    $references = UserController::getAllReferences($user->user_id);

                    $user_stat = UserStats::getLastUserStat($user->user_id);
                    if (is_null($user_stat)) {
                        $user_stat = UserStats::create(['user_id' => $user->user_id]);
                    }

                    $total_followed = People::getFollowedHourly($references);

                    $following_back = $self_info->getFollowerCount() - $user_stat->followers_count;

                    $hashtag_media_likes = LikesStats::getHashtagLikesHourly($user->user_id);
                    if ($hashtag_media_likes != 0) {
                        $hashtag_media_likes = $hashtag_media_likes - $user_stat->total_hashtag_media_likes;
                    }

                    $user_media_likes = LikesStats::getUserMediaLikesHourly($user->user_id);
                    if ($user_media_likes != 0) {
                        $user_media_likes = $user_media_likes - $user_stat->total_user_media_likes;
                    }

                    $new_user_stats = UserStats::create([
                        'user_id' => $user->user_id,
                        'followers_count' => $self_info->getFollowerCount(),
                        'following_count' => $self_info->getFollowingCount(),
                        'media_count' => $self_info->getMediaCount(),
                        'total_following_back' => $following_back,
                        'total_followed' => $total_followed,
                        'total_user_media_likes' => $user_media_likes,
                        'total_hashtag_media_likes' => $hashtag_media_likes,
                        'total_location_media_likes' => 0
                    ]);
                    sleep(90);
                }
            } catch (\Exception $ex) {
                LogController::error($user, $ex->getMessage(), 'Update Hourly Stats', $ex->getCode());
            }
        }
    }

    public static function getLastUserStat($user_id)
    {
        $user_stats = UserStats::getLastUserStat($user_id);
        if (is_null($user_stats) || !isset($user_stats)) {
            self::updateHourlyStats();
        }
        $user_stats = UserStats::getLastUserStat($user_id);
        return $user_stats;
    }

    /**
     * @throws \Exception
     */
    public function updateFollowBack()
    {
        $users = User::all();
        foreach ($users as $user) {
            self::useInstagramApi($user);
            $rank_token = InstagramController::getRankToken();
            $my_followers = $this->ig->people->getSelfFollowers($rank_token);
            $references = UserController::getAllReferences($user->user_id); //[0]->references_profiles [1]->references->hashtags
            foreach ($references["reference_profile"] as $reference) {
                $follow_back_count = self::getFollowBack($reference, $my_followers);
                ReferenceProfile::updateFollowBack($reference, $follow_back_count);
            }
            foreach ($references["reference_hashtag"] as $reference) {
                $follow_back_count = self::getFollowBack($reference, $my_followers);
                ReferenceHashtag::updateFollowBack($reference, $follow_back_count);
            }
            foreach ($references["reference_location"] as $reference) {
                $follow_back_count = self::getFollowBack($reference, $my_followers);
                ReferenceLocations::updateFollowBack($reference, $follow_back_count);
            }
        }
    }

    /**
     * @param $user
     * @throws \Exception
     */
    public function useInstagramApi($user)
    {
        $instagramController = new InstagramController($user);
        $this->ig = $instagramController->getIg();
    }

    /**
     * @param $reference
     * @param $my_followers
     * @return array|mixed
     */
    private function getFollowBack($reference, $my_followers)
    {
        try {
            $followed_people = People::getAllReferenceFollowersByDate($reference, false, true, now()->toDateString());
            if (count($followed_people) > 0) {
                $pks_f = array();
                $pks = array();
                foreach ($followed_people as $person) {
                    $pks_f[] = $person->people_id;
                }
                sleep(2);
                foreach ($my_followers->getUsers() as $item) {
                    $pks[] = $item->getPk();
                }
                foreach (array_intersect($pks, $pks_f) as $person_pk_to_update) {
                    People::where('people_id', $person_pk_to_update)->delete();
                }
                sleep(2);
                return count(array_intersect($pks, $pks_f));
            } else {
                return 0;
            }
        } catch (\Exception $ex) { }
    }

    /**
     * @param $reference
     * @param $my_followers
     * @return array|mixed
     */
    private function getFollowBackByReferenceProfile($reference, $my_followers)
    {
        try {
            $followed_people = People::getAllReferenceProfileFollowersByDate($reference, false, true, now()->toDateString());
            if (count($followed_people) > 0) {
                $pks_f = array();
                $pks = array();
                foreach ($followed_people as $person) {
                    $pks_f[] = $person->getPk();
                }
                sleep(2);
                foreach ($my_followers->getUsers() as $item) {
                    $pks[] = $item->getPk();
                }
                foreach (array_intersect($pks, $pks_f) as $person_pk_to_update) {
                    People::find($person_pk_to_update)->delete();
                }
                sleep(2);
                return count(array_intersect($pks, $pks_f));
            } else {
                return 0;
            }
        } catch (\Exception $ex) { }
    }
}
