<?php

namespace App\Http\Controllers;

use App\Models\PostMedia;
use App\Models\PostStory;
use Illuminate\Http\Request;
use Validator;

class StoryController extends Controller
{


    /**
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    public function store(Request $request)
    {
        try {
            $new_post = $this->validator($request);
            $user = auth()->userOrFail();
            $tagged_people_id = PeopleController::taggedPeople($request->tagged_people);
            $tagged_hashtag_id = HashtagController::taggedHashtag($request->tagged_hashtag);
            $media =  MediaController::store($request);
            if (!is_object($media)) return $media;
            if ($new_post['post_location']) {
                $location =  json_decode($new_post['post_location'], true);
            }
            $story_post = PostStory::updateOrCreate(
                [
                    'user_id' => $user->user_id,
                    'location_lat' => isset($location['lat']) ? $location['lat'] : null,
                    'location_lng' => isset($location['lng']) ? $location['lng'] : null,
                    'recurrent_post_frequency' => isset($new_post['recurrent_post_frequency']) ? $new_post['recurrent_post_frequency'] : null,
                    'caption' => $new_post['caption'],
                    'tagged_people_id' => $tagged_people_id,
                    'tagged_hashtag_id' => $tagged_hashtag_id,
                    'post_at' => $new_post['post_at'], //required
                ]
            );
            if ($media) {
                PostMedia::updateOrCreate(
                    [
                        'post_story_id' => $story_post->id
                    ],
                    [
                        'media_id' => $media->id,
                        'post_order' => isset($media->post_order) ? $media->post_order : 1,
                    ]
                );
            }
            return response()->json(["success" => "Post created and scheduled"], 200);
        } catch (\Exception $ex) {
            LogController::error(auth()->user(), $ex->getMessage(), 'Create new post', $ex->getCode());
            return response()->json(["error" => "Something went wrong"], 200);
        }
    }

    /**
     * @param Request $request
     * @return array|null|string
     * @throws \Exception
     */
    public function validator(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'post_at' => 'required|date',
            'photo' => 'required'
        ]);
        if ($validator->fails()) {
            throw new \Exception($validator->errors());
        } else {
            return ($request->all());
        }
    }


    /**
     * @param $tagged_pattern
     * @return array
     */
    public static function storyMentions($tagged_people)
    {
        $user_tags = [];
        $pos = [[0.95, 0.5], [0.05, 0.5]];
        $a = 0;
        $caption = [];
        foreach ($tagged_people as $person) {
            $caption[] = "@" . $person->username;
            $user_tags[] = array(
                'user_id' => $person->user_id,
                'x' => ($pos[$a][0]), 'y' => ($pos[$a][1]),
                'width' => 0.24305555, 'height' => 0.08347973,
                'rotation' => 0.0,
            );
            $a += 1;
        }
        //return array("story_mentions" => $user_tags, "caption" => implode(" ", $caption));
        return $user_tags;
    }


    /**
     * @param $tagged_pattern
     * @return mixed
     */
    public static function storyHashtag($hashtags)
    {
        if (!is_null($hashtags)) {
            $post_hashtags = [];
            $pos_x = 0;
            $pos_y = 0;
            $caption = [];
            foreach ($hashtags as $hashtag) {
                array_push($caption, "#" . $hashtag->hashtag);
                $post_hashtags[] =
                    // Note that you can add more than one hashtag in this array.
                    [
                        'tag_name' => $hashtag->hashtag, // Hashtag WITHOUT the '#'! NOTE: This hashtag MUST appear in the caption.
                        'x' => 0.5 + $pos_x, // Range: 0.0 - 1.0. Note that x = 0.5 and y = 0.5 is center of screen.
                        'y' => 0.5 + $pos_y, // Also note that X/Y is setting the position of the CENTER of the clickable area.
                        'width' => 0.24305555, // Clickable area size, as percentage of image size: 0.0 - 1.0
                        'height' => 0.07347973, // ...
                        'rotation' => 0.0,
                        'is_sticker' => false, // Don't change this value.
                        'use_custom_title' => false, // Don't change this value.
                    ];
                $pos_x += 0.1;
                $pos_y += 0.1;
            }
            return $post_hashtags;
        }
    }
}
