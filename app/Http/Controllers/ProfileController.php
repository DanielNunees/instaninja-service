<?php

namespace App\Http\Controllers;

class ProfileController extends Controller
{


    private $ig;

    /**
     * ReferenceProfileController constructor.
     * @param InstagramController $instagramController
     * @throws \Exception
     */
    public function __construct(InstagramController $instagramController)
    {
        $this->ig = $instagramController->getIg();
    }

    /**
     * Get other people's recent activities related to you and your posts.
     *
     * This feed has information about when people interact with you, such as
     * liking your posts, commenting on your posts, tagging you in photos or in
     * comments, people who started following you, etc.
     *
     * @throws \InstagramAPI\Exception\InstagramException
     *
     * /**
     * ActivityNewsResponse.
     * @return \InstagramAPI\Response\ActivityNewsResponse
     *
     * @method mixed getAdsManager()
     * @method Model\Aymf getAymf()
     * @method mixed getContinuation()
     * @method mixed getContinuationToken()
     * @method Model\Counts getCounts()
     * @method Model\Story[] getFriendRequestStories()
     * @method mixed getMessage()
     * @method Model\Story[] getNewStories()
     * @method Model\Story[] getOldStories()
     * @method mixed getPartition()
     * @method string getStatus()
     * @method Model\Subscription getSubscription()
     * @method Model\_Message[] get_Messages()
     *
     */
    public function getRecentActivityInbox()
    {
        $this->ig->people->getRecentActivityInbox();
    }

    public function getSelfInfo()
    {
        $this->ig->people->getSelfInfo()->getUser();
    }
}
