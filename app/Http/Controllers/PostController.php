<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\PostStory;
use App\Models\PostTimeline;
use App\Jobs\PostTimelinePhotoJob;
use App\Models\PostHourlyInsights;
use App\Jobs\PostStoryPhotoJob;

class PostController extends Controller
{
    private $users;
    private $ig;


    public function __construct()
    {
        $this->users = User::all();
    }

    public static function cmp($a, $b)
    {
        if ($a == $b) {
            return 0;
        }
        return ($a > $b) ? -1 : 1;
    }

    public static function checkPostAvailability($post)
    {
        $user = User::find($post->user_id);
        $date1 = now()->createFromTimeString($post->post_at);
        $date2 = now();
        if (!is_null($date1)) {
            return $date1->lessThanOrEqualTo($date2);
        } else {
            return (self::getBestHours($user->user_id));
        }
    }
    /**
     * @throws \Exception
     */
    public static function storyPost()
    {
        try {
            $story_post = PostStory::getPosts();
            foreach ($story_post as $post) {
                if (self::checkPostAvailability($post)) {
                    $user = User::find($post->user_id);
                    $post->hashtags = StoryController::storyHashtag($post->storyHashtags);
                    $post->mentions = StoryController::storyMentions($post->storyUserTags);
                    unset($post->storyUserTags);
                    unset($post->storyHashtags);
                    PostStoryPhotoJob::dispatch($user, $post); //->onQueue('posts');
                    self::updatePost($post);
                    sleep(30);
                }
            }
        } catch (\Exception $ex) {
            LogController::error(User::find($post->user_id), $ex->getMessage(), "Post Controller(story)", $ex->getCode());
        }
    }

    /**
     * @throws \Exception
     */
    public function timelinePost()
    {
        try {
            $timeline_posts = PostTimeline::getPosts();
            foreach ($timeline_posts as $post) {
                if (self::checkPostAvailability($post)) {
                    $user = User::find($post->user_id);
                    $post->user_tags = TimelineController::postUserTags($post->postUserTags);
                    unset($post->postUserTags);
                    PostTimelinePhotoJob::dispatch($user, $post); //->onQueue('posts');
                    self::updatePost($post);
                    sleep(30);
                }
            }
        } catch (\Exception $ex) {
            LogController::error(User::find($post->user_id), $ex->getMessage(), "Post Controller(Timeline)", $ex->getCode());
        }
    }

    /**
     * Return true for the top two times for current day posts
     *
     * @param $user_id
     * @return bool
     */
    public function getBestHours($user_id)
    {
        $array = PostHourlyInsights::getTodayBestHours($user_id);
        if (!is_null($array)) {
            $array = $array->toArray();
            uasort($array, array("self", 'cmp'));
            $array = array_splice($array, 0, 2);
            $array_keys = array_keys($array);

            switch (now()->hour) {
                case str_replace("H", "", $array_keys[0]):
                    return true;
                    break;
                case str_replace("H", "", $array_keys[1]):
                    return true;
                    break;
                default:
                    return false;
            }
        }
        return false;
    }




    /**
     * @param $post
     */
    public static function updatePost($new_post)
    {
        $post = PostTimeline::where('id', $new_post->id)->first();
        if (empty($post))
            $post = PostStory::where('id', $new_post->id)->first();
        switch ($post->recurrent_post_frequency) {
            case 'once_day':
                $post->already_posted = false;
                $post->post_at = now()->addDay()->toDateTimeString();
                break;
            case 'once_week':
                $post->already_posted = false;
                $post->post_at = now()->addWeek()->toDateTimeString();
                break;
            case 'once_month':
                $post->already_posted = false;
                $post->post_at = now()->addMonth()->toDateTimeString();
                break;
            default:
                $post->already_posted = true;
                $post->posted_at = now()->toDateTimeString();
                break;
        }
        $post->posted_at = now()->toDateTimeString();
        return $post->save();
    }




    /**
     * Get insights about the best hours to post my medias.
     * this data is updated every week on monday at 03:15(schedule)
     * @throws \Exception
     */
    public function postHourlyInsights()
    {
        try {
            PostHourlyInsights::truncate();
            foreach ($this->users as $user) {
                LogController::info($this->ig, "Update Insights to user " . $user->username);
                $instagramController = new InstagramController($user);
                $ig = $instagramController->getIg();
                $insights = $ig->business->getStatistics()->getData()->getUser()->getBusinessManager()->getFollowersUnit()->getDaysHourlyFollowersGraphs();
                if (!is_null($insights) && count($insights) > 0) {

                    $aux = [];
                    foreach ($insights as $insight) {
                        foreach ($insight['data_points'] as $values) {
                            $aux[$insight['name']][] = $values['value'];
                        }
                    }
                    foreach ($aux as $key => $value) {
                        PostHourlyInsights::updateInsights($ig->account_id, $key, $value);
                        sleep(1);
                    }
                }
                sleep(400);
            }
        } catch (\Exception $ex) {
            throw ($ex);
        }
    }
}
