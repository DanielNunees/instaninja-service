<?php

namespace App\Http\Controllers;

use App\Models\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;

class MediaController extends Controller
{



    /**
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function getAllMediaFromFolder(Request $request)
    {
        $folder_name = $this->validator($request);

        $directories = Storage::disk('public')->directories();
        $key = array_search($folder_name, $directories);
        $files = Storage::disk('public')->files($directories[$key]);
        $path = [];
        foreach ($files as $file) {
            $path[] = "/public/" . Storage::url($file);
        }
        return $path;
    }

    /**
     * @param Request $request
     * @return array|null|string
     * @throws \Exception
     */
    public function validator(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'folder_name' => 'required',
        ]);
        if ($validator->fails()) {
            throw new \Exception($validator->errors());
        } else {
            return $request->input('folder_name');
        }
    }

    public static function store(Request $request)
    {
        $user = auth()->userOrFail();
        if ($request->hasFile('photo')) {
            $file = $request->photo;
            $path = $request->photo->store(
                'timeline-posts/' . $user->username,
                'public'
            );
            $media = self::registerMediaOnDb();
            return $media;
        } else
            return response()->json("Media not found", 200);
    }

    /**
     *
     */
    public static function registerMediaOnDb()
    {
        $url = config('app.url');
        $directories = Storage::disk('public')->directories();
        $user = auth()->userOrFail();
        $media = "";
        foreach ($directories as $key => $dir) {
            $files = Storage::disk('public')->files($dir . "/" . $user->username);
            foreach ($files as $file) {
                $file_name = explode("/", $file)[2];
                $path = Storage::disk('public')->path($file);
                if (self::checkMedia($file_name)) {
                    $media = Media::FirstOrCreate(
                        ['path' => $path],
                        [
                            'user_id' => $user->user_id,
                            'folder_name' => $dir . "/" . $user->username,
                            'file_name' => $file_name,
                            'url' => asset('storage/' . $file),
                            'media_type' => mime_content_type($path),
                        ]
                    );
                }
            }
        }
        return $media;
    }

    /**
     * @param $file_name
     * @return bool
     */
    public static function checkMedia($file_name)
    {
        $media = Media::where('file_name', $file_name)->get();
        return $media->count() > 0 ? false : true;
    }

    public function getInfo()
    { }

    public function downloadMedia()
    {
        Storage::disk('local')->put('file.txt', 'Contents');
    }
}
