<?php

namespace App\Http\Controllers;

use App\Models\ReferenceHashtag;
use App\Models\ReferenceLocations;
use App\Models\ReferenceProfile;
use App\Models\User;
use App\Models\UserConfiguration;
use Illuminate\Http\Request;
use JWTAuth;
use Validator;
use Exception;

class UserController extends Controller
{

    private $logged_user;

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->logged_user = auth()->userOrFail();
    }

    /**
     * @return mixed
     */
    public static function getUserConfiguration($user_id)
    {
        return UserConfiguration::where('user_id', $user_id)->first();
    }



    public static function getAllReferences($user_id)
    {
        $reference_people = ReferenceProfile::getReferenceProfileUserIdList($user_id);
        $reference_hashtag = ReferenceHashtag::getReferenceHashtagUserIdList($user_id);
        $reference_location = ReferenceLocations::getReferenceLocationUserIdList($user_id);

        return array("reference_profile" => $reference_people, "reference_hashtag" => $reference_hashtag, "reference_location" => $reference_location);
    }


    public static function register($user)
    {
        try {
            $user_found = InstagramController::checkUserExistense($user->username, $user->password, $user->verification_code);
            $IG_user = $user_found->getUser();
            try {
                $new_user = User::create([
                    'user_id' => $IG_user->getPk(),
                    'email' => $user->email,
                    'username' => $user->username,
                    'ig_password' => encrypt($user->password),
                    'password' => bcrypt($user->password),
                    'profile_pic_url' => $IG_user->getProfilePicUrl(),
                    'is_active' => true
                ]);
                self::createConfiguration($IG_user->getPk());
                return response()->json("User Created!", 200);
            } catch (\Exception $ex) {
                LogController::error(null, $ex->getMessage(), 'Register User Controller', 400);
                return response()->json($ex->getMessage(), 400);
            }
        } catch (\Excpetion $ex) {
            LogController::error(null, $ex->getMessage(), 'IG Check user existence', 400);
            return response()->json($ex->getMessage(), 400);
        }
    }
    /**
     * Create default settings for new users.
     * 
     * @param User $user
     * @return void
     */
    public static function createConfiguration($user_id)
    {
        try {
            UserConfiguration::updateOrCreate(['user_id' => $user_id], [
                'throttled_request_time' => 0,
                'max_following' => 500,
                'follow_max' => 5,
                'follow_min' => 3,
                ' follow_time_max' => 150,
                ' follow_time_min' => 90,
                'unfollow_max' => 5,
                'unfollow_min' => 3,
                'un follow_time_max' => 150,
                'un follow_time_min' => 90,
                'like_max' => 5,
                'like_min' => 3,
                'like_time_max' => 150,
                'like_time_min' => 90,
                'is_active' => true,
            ]);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * @param Request $request
     * @return array|null|string
     * @throws \Exception
     */
    public function validator(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_configuration' => 'required'
        ]);
        if ($validator->fails()) {
            throw new \Exception($validator->errors());
        } else {
            return $request->input('user_configuration');
        }
    }

    public function __get($propertyName)
    {
        return $this->$propertyName;
    }

    public function allInfo()
    {
        $user = $this->ig->people->getInfoById(5600207658)->getUser();

        dd(
            $user->getAddressStreet(),
            $user->getAggregatePromoteEngagement(),
            $user->getAllowContactsSync(),
            $user->getAllowedCommenterType(),
            $user->getAutoExpandChaining(),
            $user->getBestiesCount(),
            $user->getBiography(),
            $user->getBiographyWithEntities(),
            $user->getBirthday(),
            $user->getBlockAt(),
            $user->getBusinessContactMethod(),
            $user->getByline(),
            $user->getCanBeReportedAsFraud(),
            $user->getCanBeTaggedAsSponsor(),
            $user->getCanBoostPost(),
            $user->getCanClaimPage(),
            $user->getCanConvertToBusiness(),
            $user->getCanCreateSponsorTags(),
            $user->getCanCrosspostWithoutFbToken(),
            $user->getCanFollowHashtag(),
            $user->getCanLinkEntitiesInBio(),
            $user->getCanSeeOrganicInsights(),
            $user->getCategory(),
            $user->getChainingSuggestions(),
            $user->getCityId(),
            $user->getCityName(),
            $user->getCoeffWeight(),
            $user->getContactPhoneNumber(),
            $user->getConvertFromPages(),
            $user->getCountryCode(),
            $user->getDirectMessaging(),
            $user->getEmail(),
            $user->getExternalLynxUrl(),
            $user->getExternalUrl(),
            $user->getFbPageCallToActionId(),
            $user->getFbPageCallToActionIxAppId(),
            $user->getFbPageCallToActionIxPartner(),
            $user->getFbPageCallToActionIxUrl(),
            $user->getFbuid(),
            $user->getFollowerCount(),
            $user->getFollowingCount(),
            $user->getFollowingTagCount(),
            $user->getFriendshipStatus(),
            $user->getFullName(),
            $user->getGender(),
            $user->getGeoMediaCount(),
            $user->getHasAnonymousProfilePicture(),
            $user->getHasBiographyTranslation(),
            $user->getHasChaining(),
            $user->getHasHighlightReels(),
            $user->getHasPlacedOrders(),
            $user->getHasProfileVideoFeed(),
            $user->getHasUnseenBestiesMedia(),
            $user->getHdProfilePicUrlInfo(),
            $user->getHdProfilePicVersions(),
            $user->getId(),
            $user->getIncludeDirectBlacklistStatus(),
            $user->getIsActive(),
            $user->getIsBestie(),
            $user->getIsBusiness(),
            $user->getIsCallToActionEnabled(),
            $user->getIsDirectappInstalled(),
            $user->getIsFavorite(),
            $user->getIsFavoriteForHighlights(),
            $user->getIsFavoriteForStories(),
            $user->getIsNeedy(),
            $user->getIsPrivate(),
            $user->getIsProfileActionNeeded(),
            $user->getIsUnpublished(),
            $user->getIsVerified(),
            $user->getIsVideoCreator(),
            $user->getLatestReelMedia(),
            $user->getLatitude(),
            $user->getLongitude(),
            $user->getMaxNumLinkedEntitiesInBio(),
            $user->getMediaCount(),
            $user->getMutualFollowersCount(),
            $user->getNametag(),
            $user->getNationalNumber(),
            $user->getNeedsEmailConfirm(),
            $user->getPageId(),
            $user->getPageName(),
            $user->getPermission(),
            $user->getPhoneNumber(),
            $user->getPk(),
            $user->getProfileContext(),
            $user->getProfileContextLinksWithUserIds(),
            $user->getProfileContextMutualFollowIds(),
            $user->getProfilePicId(),
            $user->getProfilePicUrl(),
            $user->getPublicEmail(),
            $user->getPublicPhoneCountryCode(),
            $user->getPublicPhoneNumber(),
            $user->getReelAutoArchive(),
            $user->getSchool(),
            $user->getScreenshotted(),
            $user->getSearchSocialContext(),
            $user->getSearchSubtitle(),
            $user->getShoppablePostsCount(),
            $user->getShowBestiesBadge(),
            $user->getShowBusinessConversionIcon(),
            $user->getShowConversionEditEntry(),
            $user->getShowFeedBizConversionIcon(),
            $user->getShowInsightsTerms(),
            $user->getShowShoppableFeed(),
            $user->getSocialContext(),
            $user->getUnseenCount(),
            $user->getUserId(),
            $user->getUsername(),
            $user->getUsertagReviewEnabled(),
            $user->getUsertagsCount(),
            $user->getZip()
        );
    }
}
