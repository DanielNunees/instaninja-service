<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use Exception;

class AuthController extends Controller
{
    public static function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string',
            'ig_password' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }


        // grab credentials from the request
        $credentials = array(
            "username" => $request->input('username'),
            "password" => decrypt($request->input('ig_password'))
        );


        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = auth()->attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        $token = response()->json(compact('token'));

        return $token;
    }

    public function register(Request $request)
    {
        try {
            $response =  UserController::register($request);
            return response()->json($response->getData(), $response->getStatusCode());
        } catch (\Exception $ex) {
            return response()->json($ex->getMessage(), 403);
        }
    }

    public function me()
    {
        try {
            $user = auth()->userOrFail();
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json(['status' => 'Token is Invalid'], 401);
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return response()->json(['status' => 'Token is Expired'], 401);
            } else {
                return response()->json(['status' => 'Authorization Token not found'], 401);
            }
        }

        return true;
    }
}
