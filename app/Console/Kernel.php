<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Models\PostTimeline;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        // $schedule->call('App\Http\Controllers\PeopleController@deleteDbFollowers')
        //     ->timezone('America/Sao_Paulo')
        //     ->dailyAt('22:00');


        $schedule->call('App\Http\Controllers\PostController@postHourlyInsights')
            ->weeklyOn(1, '01:35')
            ->timezone('Australia/Sydney');

        // $schedule->call('App\Http\Controllers\AnalyzerController@hashtagAnalyzer')
        //   ->weeklyOn(1, '03:15')
        // ->timezone('Australia/Sydney');

        $schedule->call('App\Http\Controllers\FollowersController@startFollow')
            ->everyMinute()
            ->timezone('Australia/Sydney');

        $schedule->call('App\Http\Controllers\LikesController@startLikes')
            ->everyMinute()
            ->timezone('Australia/Sydney');


        $schedule->call('App\Http\Controllers\PostController@timelinePost')
            ->everyThirtyMinutes()
            ->timezone('Australia/Sydney');

        $schedule->call('App\Http\Controllers\UserStatsController@updateFollowBack')
            ->hourly()
            ->timezone('Australia/Sydney');


        $schedule->call('App\Http\Controllers\PostController@storyPost')
            ->everyMinute()
            ->timezone('Australia/Sydney');

        $schedule->call('App\Http\Controllers\UserStatsController@updateHourlyStats')
            ->hourlyAt(6)
            ->timezone('Australia/Sydney');

        //        //Story Posts
        //        $schedule->call('App\Http\Controllers\PostController@storyPost')
        //            ->dailyAt(20)
        //            ->timezone('Australia/Sydney');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
