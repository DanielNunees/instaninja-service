<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaggedPeople extends Model
{
    protected $table = "tagged_people";
    protected $guarded = [];
}
