<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReferenceHashtag extends Model
{
    protected $guarded = [];

    public static function getRandomHashtag($user_id)
    {
        return ReferenceHashtag::where('user_id', $user_id)
            ->where('is_active', true)
            ->inRandomOrder()
            ->first();
    }

    public static function checkReferenceHashtag($user, $hashtag)
    {
        return ReferenceHashtag::where('hashtag', $hashtag)
            ->where('user_id', $user->user_id)->get();
    }

    public static function updateFollowBack($reference_hashtag_user_id, $follow_back_count)
    {
        $rp_user = ReferenceHashtag::where('reference_hashtag_user_id', $reference_hashtag_user_id)->where('is_active', true)->first();
        $rp_user->follow_back += $follow_back_count;
        $rp_user->save();
    }

    public static function incrementFollowers($reference_hashtag_user_id)
    {
        $rp_user = ReferenceHashtag::getReferenceHashtag($reference_hashtag_user_id);
        $rp_user->total_follow++;
        $rp_user->save();
    }

    public static function getReferenceHashtag($reference_hashtag_user_id)
    {
        return ReferenceHashtag::where('reference_hashtag_user_id', $reference_hashtag_user_id)->where('is_active', true)->first();
    }

    public static function getReferenceHashtagUserIdList($user_id)
    {
        $rf = ReferenceHashtag::where('user_id', $user_id)->select('reference_hashtag_user_id')->where('is_active', true)->get();
        $reference_hashtag_user_id_array = array();
        foreach ($rf as $item) {
            $reference_hashtag_user_id_array[] = $item->reference_hashtag_user_id;
        }
        return $reference_hashtag_user_id_array;
    }
}
