<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostHourlyInsights extends Model
{
    protected $guarded = [];

    public static function updateInsights($user_id, $week_day, $insights)
    {
        PostHourlyInsights::create(['week_day' => $week_day, 'user_id' => $user_id,
            'H0' => $insights[0], 'H1' => $insights[1], 'H2' => $insights[2], 'H3' => $insights[3], 'H4' => $insights[4], 'H5' => $insights[5],
            'H6' => $insights[6], 'H7' => $insights[7], 'H8' => $insights[8], 'H9' => $insights[9], 'H10' => $insights[10], 'H11' => $insights[11],
            'H12' => $insights[12], 'H13' => $insights[13], 'H14' => $insights[14], 'H15' => $insights[15], 'H16' => $insights[16], 'H17' => $insights[17],
            'H18' => $insights[18], 'H19' => $insights[19], 'H20' => $insights[20], 'H21' => $insights[21], 'H22' => $insights[22], 'H23' => $insights[23]]);
    }

    public static function getTodayBestHours($user_id)
    {
        $today = now()->englishDayOfWeek;
        return PostHourlyInsights::where('week_day', $today . "s")->where('user_id', $user_id)->select("H0", "H1", "H2", "H3", "H4", "H5", "H6", "H7", "H8", "H9",
            "H10", "H11", "H12", "H13", "H14", "H15", "H16", "H17", "H18", "H19", "H20", "H12", "H22", "H23")->first();
    }


}

