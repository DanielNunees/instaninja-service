<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReferenceLocations extends Model
{
    protected $guarded = [];


    public static function checkReferenceLocation($user, $address)
    {
        return ReferenceLocations::where('address', $address)
            ->where('user_id', $user->user_id)->get();
    }
    public static function getRandomLocation($user_id)
    {
        return ReferenceLocations::where('user_id', $user_id)
            ->where('is_active', true)
            ->inRandomOrder()
            ->first();
    }

    public static function updateFollowBack($reference_location_user_id, $follow_back_count)
    {
        $rp_user = ReferenceLocations::where('reference_location_user_id', $reference_location_user_id)->where('is_active', true)->first();
        $rp_user->follow_back += $follow_back_count;
        $rp_user->save();
    }

    public static function getReferenceLocation($reference_location_user_id)
    {
        return ReferenceLocations::where('reference_location_user_id', $reference_location_user_id)->where('is_active', true)->first();
    }

    public static function incrementFollowers($reference_location_user_id)
    {
        $rp_user = ReferenceLocations::getReferenceLocation($reference_location_user_id);
        $rp_user->total_follow++;
        $rp_user->save();
    }

    public static function getReferenceLocationUserIdList($user_id)
    {
        $rf = ReferenceLocations::where('user_id', $user_id)->select('reference_location_user_id')->where('is_active', true)->get();
        $reference_location_user_id_array = array();
        foreach ($rf as $item) {
            $reference_location_user_id_array[] = $item->reference_location_user_id;
        }
        return $reference_location_user_id_array;
    }
}
