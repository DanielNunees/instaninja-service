<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaggedHashtag extends Model
{
    protected $table = 'tagged_hashtag';
    protected $guarded = [];
}
