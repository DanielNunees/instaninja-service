<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SystemLog extends Model
{
    protected $table = "system_logs";
    protected $guarded = [];


    public static function newLog($user_id, $username, $message, $message_type)
    {
        return ExceptionsLog::create([
            'user_id' => $user_id,
            'username' => $username,
            'message' => $message,
            'message_type' => $message_type,
        ]);
    }
}
