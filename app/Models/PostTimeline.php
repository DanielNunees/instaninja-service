<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostTimeline extends Model
{
    protected $table = "post_timeline";
    protected $guarded = [];
    protected $with = ['postMedia', 'postUserTags'];



    public function postMedia()
    {
        return $this->belongsToMany(Media::class, 'post_media', 'post_timeline_id', 'media_id');
    }

    public function postUserTags()
    {
        return $this->hasMany(TaggedPeople::class, 'tagged_people_id', 'tagged_people_id');
    }

    public static function getAlreadyNotPosted()
    {
        return PostTimeline::where('already_posted', false)->count() > 4 ? true : false;
    }

    public static function getPosts()
    {
        return PostTimeline::where('already_posted', false)->get();
    }

    public static function resetPosts()
    {
        return PostTimeline::where('already_posted', true)->update(['already_posted' => true]);
    }


    public function comments()
    {
        return $this->hasMany('App\Models\Comments', 'id', 'comment_id')->get();
    }

    public function location()
    {
        return $this->hasOne('App\Models\Location', 'id', 'location_id')->first();
    }
}
