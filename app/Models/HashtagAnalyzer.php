<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class HashtagAnalyzer extends Model
{
    protected $table = "hashtag_analyzer";
    protected $guarded = [];


    public static function getHashtagCount()
    {
        return DB::table('hashtag_analyzer')->select("hashtag", DB::raw('COUNT(hashtag) as quantity'))->orderByDesc('quantity')->groupBy("hashtag")->havingRaw("COUNT(hashtag) > 1")->get();
    }
}
