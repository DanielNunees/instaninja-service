<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LikesStats extends Model
{
    protected $table = 'likes_stats';

    public static function getHashtagLikesHourly($user_id)
    {
        return LikesStats::where('user_id', $user_id)
            ->where('type', "hashtag")
            ->sum('total_likes');
    }


    public static function getUserMediaLikesHourly($user_id)
    {
        return LikesStats::where('user_id', $user_id)
            ->where('type', "feed_timeline")
            ->sum('total_likes');
    }
}
