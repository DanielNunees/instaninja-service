<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExceptionsLog extends Model
{
    protected $guarded = [];

    public static function newLog($user_id, $username, $exception, $exception_type, $code)
    {
        return ExceptionsLog::create([
            'user_id' => $user_id,
            'username' => $username,
            'exception' => $exception,
            'exception_type' => $exception_type,
            'code' => $code]);
    }
}
