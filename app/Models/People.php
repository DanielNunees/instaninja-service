<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    protected $guarded = [];

    public static function isNewPeople($people, $reference)
    {
        return People::where('people_id', $people->getPk())
            ->where('username', $people->getUsername())
            ->whereRaw('(reference_profile_user_id = "' . $reference . '" or reference_hashtag_user_id = "' . $reference . '" or reference_location_user_id = "' . $reference . '")')
            ->first();
    }



    public static function peopleToFollow($reference_array, $min, $max)
    {
        $rf_people = People::whereRaw('(reference_profile_user_id IN (' . "'" . implode("','", $reference_array['reference_profile']) . "'" . ') or reference_hashtag_user_id IN (' . "'" . implode("','", $reference_array['reference_hashtag']) . "'" . ') or reference_location_user_id IN (' . "'" . implode("','", $reference_array['reference_location']) . "'" . '))')
            ->where('friendship_status', false)
            ->where('is_active', false)
            ->take(mt_rand($min, $max))
            ->distinct()
            ->get();


        return $rf_people;
    }

    public static function deletePeopleById($id)
    {
        People::where('people_id', $id)->delete();
    }

    public static function peopleToUnfollow($reference_array, $min, $max)
    {
        $rf_people = People::whereRaw('(reference_profile_user_id IN (' . "'" . implode("','", $reference_array['reference_profile']) . "'" . ') or reference_hashtag_user_id IN (' . "'" . implode("','", $reference_array['reference_hashtag']) . "'" . ') or reference_location_user_id IN (' . "'" . implode("','", $reference_array['reference_location']) . "'" . '))')
            ->whereRaw("ADDTIME(updated_at,'8:0') <= CURRENT_TIME()")
            ->where('friendship_status', true)
            ->where('is_active', true)
            ->take(mt_rand($min, $max))
            ->distinct()
            ->inRandomOrder()
            ->get();


        return $rf_people;
    }

    public static function getAllReferenceFollowers($reference_array, $friendship_status, $is_active)
    {
        return People::whereRaw('(reference_profile_user_id IN (' . "'" . implode("','", $reference_array['reference_profile']) . "'" . ') or reference_hashtag_user_id IN (' . "'" . implode("','", $reference_array['reference_hashtag']) . "'" . ') or reference_location_user_id IN (' . "'" . implode("','", $reference_array['reference_location']) . "'" . '))')
            ->where('friendship_status', $friendship_status)
            ->where('is_active', $is_active)
            ->orderBy("created_at", "asc")
            ->get();
    }

    public static function getAllReferenceFollowersByDate($reference, $friendship_status, $is_active, $date)
    {
        return People::whereRaw('(reference_profile_user_id = "' . $reference . '" or reference_hashtag_user_id = "' . $reference . '" or reference_location_user_id = "' . $reference . '")')
            ->whereDate('updated_at', '>=', $date)
            ->where('friendship_status', $friendship_status)
            ->where('is_active', $is_active)
            ->distinct()
            ->get();
    }


    public static function getFollowedHourly($reference_array)
    {
        return People::whereRaw('(reference_profile_user_id IN (' . "'" . implode("','", $reference_array['reference_profile']) . "'" . ') or reference_hashtag_user_id IN (' . "'" . implode("','", $reference_array['reference_hashtag']) . "'" . ') or reference_location_user_id IN (' . "'" . implode("','", $reference_array['reference_location']) . "'" . '))')
            ->where('updated_at', '>=', now()->subHour()->toDateTimeString())
            ->where('friendship_status', true)
            ->where('is_active', true)->count();
    }



    public static function deletePeople($reference_profile_user_id)
    {
        People::where('reference_profile_user_id', $reference_profile_user_id)
            ->where('friendship_status', true)->delete();
    }

    public static function findFollower($people_id, $reference_array)
    {
        return People::whereRaw('(reference_profile_user_id IN (' . "'" . implode("','", $reference_array['reference_profile']) . "'" . ') or reference_hashtag_user_id IN (' . "'" . implode("','", $reference_array['reference_hashtag']) . "'" . ') or reference_location_user_id IN (' . "'" . implode("','", $reference_array['reference_location']) . "'" . '))')
            ->where('people_id', $people_id)
            ->orderBy("created_at", "asc")
            ->first();
    }
}
