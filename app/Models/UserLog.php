<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{
    protected $guarded = [];

    public static function newLog($user_id, $username, $message)
    {
        return UserLog::create([
            'user_id' => $user_id, 'username' => $username,
            'message' => $message
        ]);
    }
}
