<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActionSchedule extends Model
{
    protected $table = "actions_schedule";
    protected $guarded = [];
}
