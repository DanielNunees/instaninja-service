<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserStats extends Model
{

    protected $fillable = [
        'followers_count', 'following_count', 'media_count', 'user_id',
        'total_followed', 'total_following_back', 'total_user_media_likes',
        'total_hashtag_media_likes', 'total_location_media_likes'
    ];
    private $following_count;
    private $total_user_media_likes;
    private $total_hashtag_media_likes;
    private $total_location_media_likes;
    private $media_count;
    private $total_followed;
    private $user_id;
    private $followers_count;
    private $total_following_back;

    public static function getLastUserStat($user_id)
    {
        return UserStats::where('user_id', $user_id)->orderBy('updated_at', 'desc')->first();
    }

    public static function getUserStats($user_id)
    {
        return UserStats::where('user_id', $user_id)->orderBy('updated_at', 'desc')->take(50)->get();
    }

    public static function getDailyStats($user_id)
    {
        return array(
            "sum" =>
            UserStats::where('user_id', $user_id)
                ->whereDate('created_at', '>=', now()->toDateString())
                ->select(
                    DB::raw('SUM(total_following_back) as total_following_back,SUM(total_followed) as total_followed,SUM(total_user_media_likes) as total_user_media_likes,SUM(total_hashtag_media_likes) as total_hashtag_media_likes,SUM(total_location_media_likes) as total_location_media_likes')
                )
                ->orderBy('created_at', 'DSC')
                ->groupBy('user_id')->get(),
            "values" =>
            UserStats::where('user_id', $user_id)
                ->select('followers_count', 'following_count', 'media_count')
                ->whereDate('created_at', '>=', now()->toDateString())
                ->latest()->first(),

        );
    }

    public static function getNewDailyFollowersCount($user_id)
    {
        return UserStats::where('user_id', $user_id)
            ->where('updated_at', '>=', now()->toDateString())
            ->where('is_active', true)
            ->sum('follow_back');
    }

    /**
     * @return mixed
     */
    public function getFollowingCount()
    {
        return $this->following_count;
    }

    /**
     * @param mixed $following_count
     */
    public function setFollowingCount($following_count): void
    {
        $this->following_count = $following_count;
    }

    /**
     * @return mixed
     */
    public function getTotalUserMediaLikes()
    {
        return $this->total_user_media_likes;
    }

    /**
     * @param mixed $total_user_media_likes
     */
    public function setTotalUserMediaLikes($total_user_media_likes): void
    {
        $this->total_user_media_likes = $total_user_media_likes;
    }

    /**
     * @return mixed
     */
    public function getTotalHashtagMediaLikes()
    {
        return $this->total_hashtag_media_likes;
    }

    /**
     * @param mixed $total_hashtag_media_likes
     */
    public function setTotalHashtagMediaLikes($total_hashtag_media_likes): void
    {
        $this->total_hashtag_media_likes = $total_hashtag_media_likes;
    }

    /**
     * @return mixed
     */
    public function getTotalLocationMediaLikes()
    {
        return $this->total_location_media_likes;
    }

    /**
     * @param mixed $total_location_media_likes
     */
    public function setTotalLocationMediaLikes($total_location_media_likes): void
    {
        $this->total_location_media_likes = $total_location_media_likes;
    }

    /**
     * @return mixed
     */
    public function getMediaCount()
    {
        return $this->media_count;
    }

    /**
     * @param mixed $media_count
     */
    public function setMediaCount($media_count): void
    {
        $this->media_count = $media_count;
    }

    /**
     * @return mixed
     */
    public function getTotalFollowed()
    {
        return $this->total_followed;
    }

    /**
     * @param mixed $total_followed
     */
    public function setTotalFollowed($total_followed): void
    {
        $this->total_followed = $total_followed;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getFollowersCount()
    {
        return $this->followers_count;
    }

    /**
     * @param mixed $followers_count
     */
    public function setFollowersCount($followers_count): void
    {
        $this->followers_count = $followers_count;
    }

    /**
     * @return mixed
     */
    public function getTotalFollowingBack()
    {
        return $this->total_following_back;
    }

    /**
     * @param mixed $total_following_back
     */
    public function setTotalFollowingBack($total_following_back): void
    {
        $this->total_following_back = $total_following_back;
    }
}
