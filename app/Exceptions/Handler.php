<?php

namespace App\Exceptions;

use App\Http\Controllers\LogController;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];
    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        if ($exception instanceof \InstagramAPI\Exception\AccountDisabledException) {
            Log::error("AccountDisabledException");
            return;
        }
        if ($exception instanceof \InstagramAPI\Exception\BadRequestException) {
            Log::error("BadRequestException " . $exception->getMessage());
            return;
        }
        if ($exception instanceof \InstagramAPI\Exception\ChallengeRequiredException) {
            Log::error("ChallengeRequiredException " . $exception->getMessage());
        }
        if ($exception instanceof \InstagramAPI\Exception\CheckpointRequiredException) {
            Log::error("CheckpointRequiredException " . $exception->getMessage());
        }
        if ($exception instanceof \InstagramAPI\Exception\ConsentRequiredException) {
            Log::error("ConsentRequiredException " . $exception->getMessage());
        }
        if ($exception instanceof \InstagramAPI\Exception\EmptyResponseException) {
            Log::error("EmptyResponseException " . $exception->getMessage());
            if ($exception->hasResponse()) { // <-- VERY IMPORTANT TO CHECK FIRST!
                Log::error($exception->getResponse()->printJson());
                Log::error($exception->getResponse()->getMessage());
            }
            sleep(60);
        }
        if ($exception instanceof \InstagramAPI\Exception\EndpointException) {
            LogController::error(null, $exception->getMessage(), $exception->getResponse(), $exception->getCode());
            if ($exception->hasResponse()) { // <-- VERY IMPORTANT TO CHECK FIRST!
                Log::error($exception->getResponse()->printJson());
                Log::error($exception->getResponse()->getMessage());
            }
        }
        if ($exception instanceof \InstagramAPI\Exception\FeedbackRequiredException) {
            Log::error("FeedbackRequiredException " . $exception->getMessage());
            Log::error($exception);
            sleep(300);
        }
        if ($exception instanceof \InstagramAPI\Exception\ForcedPasswordResetException) {
            Log::error("ForcedPasswordResetException");
        }
        if ($exception instanceof \InstagramAPI\Exception\IncorrectPasswordException) {
            Log::error("IncorrectPasswordException");
        }
        if ($exception instanceof \InstagramAPI\Exception\InternalException) {
            Log::error("InternalException " . $exception->getMessage());
        }
        if ($exception instanceof \InstagramAPI\Exception\InvalidSmsCodeException) {
            Log::error("InvalidSmsCodeException");
        }
        if ($exception instanceof \InstagramAPI\Exception\InvalidUserException) {
            Log::error("InvalidUserException");
        }
        if ($exception instanceof \InstagramAPI\Exception\LoginRequiredException) {
            Log::error("LoginRequiredException]: " . $exception->getMessage());
        }
        if ($exception instanceof \InstagramAPI\Exception\InvalidSmsCodeException) {
            Log::error("InvalidSmsCodeException");
        }
        if ($exception instanceof \InstagramAPI\Exception\NetworkException) {
            Log::error("NetworkException");
        }
        if ($exception instanceof \InstagramAPI\Exception\NotFoundException) {
            Log::error("NotFoundException " . $exception->getMessage());
        }
        if ($exception instanceof \InstagramAPI\Exception\RequestException) {
            Log::error("RequestException");
        }
        if ($exception instanceof \InstagramAPI\Exception\RequestHeadersTooLargeException) {
            Log::error("RequestHeadersTooLargeException");
        }
        if ($exception instanceof \InstagramAPI\Exception\SentryBlockException) {
            Log::error("SentryBlockException");
        }
        if ($exception instanceof \InstagramAPI\Exception\ServerMessageThrower) {
            Log::error("ServerMessageThrower " . $exception->getMessage());
            return;
        }
        if ($exception instanceof \InstagramAPI\Exception\SettingsException) {
            Log::error("SettingsException");
        }
        if ($exception instanceof \InstagramAPI\Exception\ThrottledException) {
            Log::error("ThrottledException " . $exception->getMessage());
            return;
        }
        if ($exception instanceof \InstagramAPI\Exception\UploadFailedException) {
            Log::error("UploadFailedException");
        }
        if ($exception instanceof \InstagramAPI\Exception\InstagramException) {
            Log::error("InstagramException");
        }
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }
}
