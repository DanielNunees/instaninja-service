<?php

namespace App\Exceptions;

use Exception;

class InstagramException extends Exception
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report the exception.
     *
     * @param $exception
     * @return void
     */
    public function report()
    {
        dd(1);
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request, $exception)
    {

        return parent::render($request, $exception);
    }
}
