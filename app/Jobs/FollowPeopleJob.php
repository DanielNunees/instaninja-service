<?php

namespace App\Jobs;

use App\Http\Controllers\HashtagController;
use App\Http\Controllers\InstagramController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\ReferenceProfileController;
use App\Http\Controllers\UserController;
use App\Models\People;
use App\Models\User;
use App\Models\UserConfiguration;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;

class FollowPeopleJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;
    public $tries = 1;
    public $timeout = 2000;
    protected $user;
    protected $ig;
    protected $reference_profile;
    private $user_configuration;
    private $throttled_request_time_count = 0;

    /**
     * Create a new job instance.
     *
     * @param  User $user
     * @param UserConfiguration $user_configuration
     */
    public function __construct(User $user, UserConfiguration $user_configuration)
    {
        $this->user = $user;
        $this->user_configuration = $user_configuration;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $instagramController = new InstagramController($this->user);
        $this->ig = $instagramController->getIg();
        $this->followListPeople();
    }

    /**
     * Follow a list of People
     *
     *
     * @return void
     * @throws \Exception
     */
    public function followListPeople()
    {
        $aux = UserController::getAllReferences($this->ig->account_id);
        $peopleRF = People::peopleToFollow($aux, $this->user_configuration->follow_min, $this->user_configuration->follow_max);
        LogController::info($this->ig, "Follow min: " . $this->user_configuration->follow_min . "  follow max: " . $this->user_configuration->follow_max);
        foreach ($peopleRF as $person) {
            try {
                if (isset($person->people_id)) {
                    sleep(15);
                    $response = self::follow($person->people_id);
                    $person->friendship_status = true;
                    $person->is_active = true;
                    LogController::info($this->ig, "Follow PeopleID: " . $person->people_id . ", Status: " . $response);
                    $person->save();
                    self::incrementFollowers($person);
                }
            } catch (\Exception $ex) {
                LogController::error($this->user, $ex->getMessage(), "Follow List People Job", $ex->getCode());
            }
        }
        self::updateThrottledRequest();
        return;
    }

    /**
     *  Follow a single person
     * @param int $user_id
     * @throws \Exception
     */
    public function follow($user_id)
    {
        try {
            sleep(15);
            $response = $this->ig->people->follow($user_id);
            return $response->getStatus();
        } catch (\Exception $ex) {
            self::throttledTime();
            People::deletePeopleById($user_id);
            throw $ex;
        }
    }

    /**
     *  If we had three unsuccessful follows the throttled_request_time is increased in 30 min
     * @param int $response
     */
    public function throttledTime()
    {
        $this->throttled_request_time_count += 1;
        if ($this->throttled_request_time_count == 3) {
            $this->user_configuration->throttled_request_time += 30;
            $this->user_configuration->save();
        }
    }

    /**
     *  If we had three successful follows the throttled_request_time is decreased in 30 min
     * @param int $response
     */
    public function updateThrottledRequest()
    {
        if ($this->throttled_request_time_count < 3) {
            $this->user_configuration->throttled_request_time == 0 ? $this->user_configuration->throttled_request_time = 0 : $this->user_configuration->throttled_request_time -= 30;
            $this->user_configuration->save();
        }

        $this->throttled_request_time_count = 0;
    }

    public function incrementFollowers($person)
    {
        if (!is_null($person->reference_profile_user_id) && $person->reference_profile_user_id != "") {
            ReferenceProfileController::incrementFollowers($person->reference_profile_user_id);
        }

        if (!is_null($person->reference_hashtag_user_id) && $person->reference_hashtag_user_id != "") {
            HashtagController::incrementFollowers($person->reference_hashtag_user_id);
        }

        if (!is_null($person->reference_location_user_id) && $person->reference_location_user_id != "") {
            LocationController::incrementFollowers($person->reference_location_user_id);
        }
    }
}
