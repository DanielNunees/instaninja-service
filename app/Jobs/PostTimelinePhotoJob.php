<?php

namespace App\Jobs;

use App\Http\Controllers\InstagramController;
use App\Http\Controllers\LogController;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\PostTimeline;

class PostTimelinePhotoJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;
    public $tries = 1;
    public $timeout = 5000;
    protected $user;
    protected $ig;
    private $post;

    /**
     * Create a new job instance.
     * @param User $user
     * @param PostTimeline $post
     */
    public function __construct(User $user, PostTimeline $post)
    {
        $this->user = $user;
        $this->post = $post;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        try {
            $instagramController = new InstagramController($this->user);
            $this->ig = $instagramController->getIg();

            if (count($this->post->postMedia) > 1) {
                self::postTimelineAlbum();
            } else {
                self::postTimelinePhoto($this->post);
            }
        } catch (\Exception $ex) {
            LogController::error($this->user, $ex->getMessage(), "Timeline post Job", $ex->getCode());
        }
    }

    /**
     * @throws \Exception
     */
    public function postTimelineAlbum()
    {
        $metadata = self::checkParameters();
        //////////////////////

        ////// NORMALIZE MEDIA //////
        // All album files must have the same aspect ratio.
        // We copy the app's behavior by using the first file
        // as template for all subsequent ones.
        $mediaOptions = [
            'targetFeed' => \InstagramAPI\Constants::FEED_TIMELINE_ALBUM,
            // Uncomment to expand media instead of cropping it.
            //'operation' => \InstagramAPI\Media\InstagramMedia::EXPAND,
        ];
        foreach ($metadata['media'] as &$item) {
            /** @var \InstagramAPI\Media\InstagramMedia|null $validMedia */
            $validMedia = null;
            switch ($item['type']) {
                case 'image/png':
                    $validMedia = new \InstagramAPI\Media\Photo\InstagramPhoto($item['path'], $mediaOptions);
                    break;
                case 'video':
                    $validMedia = new \InstagramAPI\Media\Video\InstagramVideo($item['path'], $mediaOptions);
                    break;
                default:
                    // Ignore unknown media type.
            }
            if ($validMedia === null) {
                continue;
            }
            try {
                $item['path'] = $validMedia->getFile();
                // We must prevent the InstagramMedia object from destructing too early,
                // because the media class auto-deletes the processed file during their
                // destructor's cleanup (so we wouldn't be able to upload those files).
                $item['__media'] = $validMedia; // Save object in an unused array key.
            } catch (\Exception $ex) {
                LogController::error($this->user->user_id, $ex->getMessage(), "Timeline post Album Check parameters", $ex->getCode());

                continue;
            }
            if (!isset($mediaOptions['forceAspectRatio'])) {
                // Use the first media file's aspect ratio for all subsequent files.
                /** @var \InstagramAPI\Media\MediaDetails $mediaDetails */
                $mediaDetails = $validMedia instanceof \InstagramAPI\Media\Photo\InstagramPhoto
                    ? new \InstagramAPI\Media\Photo\PhotoDetails($item['file'])
                    : new \InstagramAPI\Media\Video\VideoDetails($item['file']);
                $mediaOptions['forceAspectRatio'] = $mediaDetails->getAspectRatio();
            }
        }
        unset($item);
        try {
            //  $this->ig->timeline->uploadAlbum($this->post->post_media_values, ['caption' => $captionText]);
        } catch (\Exception $ex) {
            LogController::error($this->user, $ex->getMessage(), "Timeline post Album Check parameters", $ex->getCode());
        }
    }

    public function getLocation($lat, $lng)
    {
        try {
            if (isset($lat) && isset($lng)) return $this->ig->location->search($lat, $lng)->getVenues()[0];
            else return null;
        } catch (\Exception $ex) {
            LogController::error($this->user, $ex->getMessage(), "Location not found", $ex->getCode());
            return null;
        }
    }

    /**
     * @throws \Exception
     */
    public function postTimelinePhoto($post)
    {
        try {
            $photo = new \InstagramAPI\Media\Photo\InstagramPhoto($post->postMedia[0]->path);
            $location = self::getLocation($post->location_lat, $post->location_lng);
            $post_media = $this->ig->timeline->uploadPhoto(
                $photo->getFile(),
                [
                    'caption' => $post->caption,
                    'usertags' => $post->user_tags,
                    'location' => $location
                ]
            );
            sleep(45);
            $media_id = ($post_media->getMedia()->getId());
            $this->ig->media->comment($media_id, $post->comment);

            LogController::info($this->ig, "Timeline Post");
        } catch (\Exception $ex) {
            LogController::error($this->user, $ex->getMessage(), "Timeline Post", $ex->getCode());
            throw $ex;
        }
    }
}
