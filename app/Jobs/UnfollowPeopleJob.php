<?php

namespace App\Jobs;

use App\Http\Controllers\InstagramController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\UserController;
use App\Models\People;
use App\Models\User;
use App\Models\UserConfiguration;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;

class UnfollowPeopleJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;
    public $tries = 1;
    public $timeout = 2000;
    protected $user;
    protected $ig;
    private $user_configuration;

    /**
     * Create a new job instance.
     *
     * @param  User $user
     * @param UserConfiguration $user_configuration
     */
    public function __construct(User $user, UserConfiguration $user_configuration)
    {
        $this->user = $user;
        $this->user_configuration = $user_configuration;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        try {
            $instagramController = new InstagramController($this->user);
            $this->ig = $instagramController->getIg();
            $this->unfollowListPeople();
        } catch (\Exception $ex) {
            LogController::error($this->user, $ex->getMessage(), "Unfollow List People Job", $ex->getCode());
        }
    }

    /**
     * Unfollow a list of People
     *
     *
     * @return void
     * @throws \Exception
     */
    public function unfollowListPeople()
    {
        $aux = UserController::getAllReferences($this->ig->account_id);
        $peopleRF = People::peopleToUnfollow($aux, $this->user_configuration->unfollow_min, $this->user_configuration->unfollow_max);
        LogController::info($this->ig, "Unfollow min: " . $this->user_configuration->unfollow_min . "  Unfollow max: " . $this->user_configuration->unfollow_max);
        foreach ($peopleRF as $person) {
            try {
                if (isset($person->people_id)) {
                    sleep(15);
                    $this->unfollow($person->people_id);
                    $person->friendship_status = false;
                    $person->save();
                    LogController::info($this->ig, "Unfollow PeopleID: " . $person->people_id);
                }
            } catch (\Exception $ex) {
                throw $ex;
            }
        }
    }

    /**
     *  Unfollow a single person
     * @param int $user_id
     */
    public function unfollow($user_id)
    {
        try {
            sleep(15);
            $this->ig->people->unfollow($user_id);
        } catch (\Exception $ex) {
            People::deletePeopleById($user_id);
            throw $ex;
        }
    }
}
