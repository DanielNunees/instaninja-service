<?php

namespace App\Jobs;

use App\Models\People;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class DeleteDbFollowersJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;
    public $tries = 1;
    public $timeout = 500;
    protected $reference_profile;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ReferenceProfile $referenceProfile)
    {
        $this->reference_profile = $referenceProfile;
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            People::deletePeoples($this->reference_profile->reference_profile_user_id);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
        }
    }
}
