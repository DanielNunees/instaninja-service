<?php

namespace App\Jobs;

use App\Http\Controllers\InstagramController;
use App\Http\Controllers\LogController;
use App\Models\PostAttributes;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\PostStory;

class PostStoryPhotoJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;
    public $tries = 1;
    public $timeout = 5000;
    protected $user;
    protected $ig;
    private $post;

    /**
     * Create a new job instance.
     * @param User $user
     * @param PostAttributes $post_atributtes
     */
    public function __construct(User $user, PostStory $post)
    {
        $this->user = $user;
        $this->post = $post;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $instagramController = new InstagramController($this->user);
        $this->ig = $instagramController->getIg();
        $this->postStoryPhoto($this->post);
    }

    public function getLocation($lat, $lng)
    {
        try {
            if (isset($lat) && isset($lng)) {
                $fb_location = $this->ig->location->search($lat, $lng)->getVenues()[0];
                $location['location'] = $fb_location;
                $location['location_sticker'] = [
                    'width'         => 0.89333333333333331,
                    'height'        => 0.071281859070464776,
                    'x'             => 0.5,
                    'y'             => 0.2,
                    'rotation'      => 0.0,
                    'is_sticker'    => true,
                    'location_id'   => $fb_location->getExternalId(),
                ];
                return $location;
            } else
                return null;
        } catch (\Exception $ex) {
            LogController::error($this->user, $ex->getMessage(), "Location not found", $ex->getCode());
            return null;
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function postStoryPhoto($post)
    {
        try {
            $photo = new \InstagramAPI\Media\Photo\InstagramPhoto($post->postMedia[0]->path, ['targetFeed' => \InstagramAPI\Constants::FEED_STORY]);

            $location = self::getLocation($post->location_lat, $post->location_lng);
            $this->ig->story->uploadPhoto(
                $photo->getFile(),
                [
                    'caption' => $post->caption,
                    'usertags' => $post->mentions,
                    'hashtags' => $post->hashtags['hashtag'],
                    'location' => $location['location'],
                    'location_sticker' => $location['location_sticker']
                ]
            );
            LogController::info($this->ig, "Timeline Post");
        } catch (\Exception $ex) {
            LogController::error($this->user, $ex->getMessage(), "Story Post", $ex->getCode());
            throw $ex;
        }
    }
}
