<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use App\Http\Controllers\InstagramController;
use App\Http\Controllers\LogController;
use App\Models\LikesStats;
use App\Http\Controllers\TimelineController;
use App\Http\Controllers\HashtagController;
use App\Http\Controllers\LocationController;
use App\Models\UserConfiguration;

class LikeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;
    public $tries = 1;
    public $timeout = 500;
    protected $user;
    protected $ig;
    private $hashtag_controller;
    private $location_controller;
    protected $reference_profile;
    private $user_configuration;
    private $throttled_request_time_count = 0;

    /**
     * Create a new job instance.
     *
     * @param  User $user
     * @param UserConfiguration $user_configuration
     */
    public function __construct(User $user, UserConfiguration $user_configuration)
    {
        $this->user = $user;
        $this->user_configuration = $user_configuration;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $instagramController = new InstagramController($this->user);
        $this->hashtag_controller = new HashtagController($instagramController);
        $this->location_controller = new LocationController($instagramController);
        $this->ig = $instagramController->getIg();

        $value = mt_rand(1, 3);
        switch ($value) {
            case 1:
                self::likeMyFeed();
                break;
            case 2:
                self::likeByHashTag();
                break;
            case 3:
                self::likeByLocation();
                break;
        }
    }

    /**
     *
     */
    public function likeMyFeed()
    {
        try {
            $filtered_media = TimelineController::getFeedMedia($this->user);
            LogController::info($this->user, "Like My Feed: " . count($filtered_media));
            if (!is_null($filtered_media)) {
                foreach ($filtered_media as $media) {
                    LogController::info($this->user, "Username: " . $media->getMediaOrAd()->getUser()->getUsername());
                    $media_id = $media->getMediaOrAd()->getId();
                    $username = $media->getMediaOrAd()->getUser()->getUsername();
                    sleep(15);
                    self::likeMedia($media_id);
                    self::updateLikes('feed_timeline', $username);
                }
            }
            self::updateThrottledRequest();
            return;
        } catch (\Exception $ex) {
            LogController::error($this->user, $ex->getMessage(), 'Like My Feed Job', $ex->getCode());
        }
    }

    /**
     * @param $hashtag_rank_token
     */
    public function likeByHashTag()
    {
        try {
            $hashtag = $this->hashtag_controller->getRandom($this->user);
            if (is_null($hashtag)) {
                self::likeMyFeed();
                return;
            }
            $filtered_media = $this->hashtag_controller->getHashtagMedia($hashtag);
            LogController::info($this->user, "Like Hashtag: " . $hashtag->hashtag);
            if (!is_null($filtered_media)) {
                foreach ($filtered_media as $media) {
                    $media_id = $media->getId();
                    sleep(15);
                    self::likeMedia($media_id);
                    self::updateLikes('hashtag', $hashtag->hashtag);
                }
            } else {
                self::likeMyFeed();
            }
            self::updateThrottledRequest();
            return;
        } catch (\Exception $ex) {
            LogController::error($this->user, $ex->getMessage(), 'Like Hashtag Job', $ex->getCode());
        }
    }

    public function likeByLocation()
    {
        try {
            $location = $this->location_controller->getRandom($this->user);
            if (!is_null($location)) {
                LogController::info($this->ig, "Dispatch location like: " . $location->address);
                $filtered_media = $this->location_controller->getLocationMedia($location, $this->user);
                if (!is_null($filtered_media)) {
                    foreach ($filtered_media as $media) {
                        $media_id = $media->getId();
                        self::likeMedia($media_id);
                        self::updateLikes('location', $location->address);
                    }
                } else {
                    self::likeMyFeed();
                }
            } else {
                self::likeMyFeed();
            }
            self::updateThrottledRequest();
            return;
        } catch (\Exception $ex) {
            LogController::error($this->user, $ex->getMessage(), 'Like By Location', $ex->getCode());
        }
    }

    /**
     * @param $module
     * @param $value
     */
    private function updateLikes($module, $value)
    {
        try {

            $user_likes_stats = LikesStats::where('user_id', $this->ig->account_id)
                ->where('type', $module)
                ->where('value', $value)->first();

            if (is_null($user_likes_stats)) {

                $likes_stats = new LikesStats();
                $likes_stats->user_id = $this->ig->account_id;
                $likes_stats->type = $module;
                $likes_stats->value = $value;
                $likes_stats->total_likes = 1;
                $likes_stats->save();
            } else {
                $user_likes_stats->total_likes += 1;
                $user_likes_stats->save();
            }
        } catch (\Exception $ex) {
            LogController::error($this->user, $ex->getMessage(), 'Update Likes', $ex->getCode());
        }
    }

    /**
     * @param $media_id
     */
    private function likeMedia($media_id)
    {
        try {
            sleep(15);
            $like = $this->ig->media->like($media_id);
        } catch (\Exception $ex) {
            sleep(45);
            self::throttledTime();
            LogController::error($this->user, $ex->getMessage(), 'Like Media', $ex->getCode());
            throw $ex;
        }
    }

    /**
     *  If we had three successful follows the throttled_request_time is decreased in 30 min
     * @param int $response
     */
    public function updateThrottledRequest()
    {
        if ($this->throttled_request_time_count < 3) {
            $this->user_configuration->throttled_request_time == 0 ? $this->user_configuration->throttled_request_time = 0 : $this->user_configuration->throttled_request_time -= 30;
            $this->user_configuration->save();
        }

        $this->throttled_request_time_count = 0;
    }

    /**
     *  If we had three follows unsucefull the throttled_request_time is increased in 30 min
     * @param int $response
     */
    public function throttledTime()
    {
        $this->throttled_request_time_count += 1;
        if ($this->throttled_request_time_count == 3) {
            $this->user_configuration->throttled_request_time += 30;
            $this->user_configuration->save();
        }
    }
}
