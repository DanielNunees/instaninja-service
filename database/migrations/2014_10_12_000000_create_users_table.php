<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->engine = 'InnoDB';
                $table->unsignedBigInteger('user_id');
                $table->string('email')->unique();
                $table->primary('user_id');
                $table->string('username')->unique();
                $table->longText('password');
                $table->longText('ig_password');
                $table->longText('profile_pic_url')->nullable();
                $table->longText('api_session_token')->nullable();
                $table->boolean('is_active')->nullable();
                $table->timestamps();
            });
        } else {
            Schema::table('users', function (Blueprint $table) {
                if (!Schema::hasColumn('users', 'email', 'ig_password')) {
                    $table->string('email')->unique()->after('user_id');
                    $table->string('ig_password')->unique()->after('password');
                }
                if (!Schema::hasColumn('users', 'profile_pic_url')) {
                    $table->longText('profile_pic_url')->nullable()->after('ig_password');
                }
                if (!Schema::hasColumn('users', 'api_session_token')) {
                    $table->longText('api_session_token')->after('profile_pic_url')->nullable();
                }
                // if (Schema::hasColumn('users', 'password_string')) {
                //     $table->drop('password_string');
                // }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
