<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        if (!Schema::hasTable('system_logs')) {
            Schema::create('system_logs', function (Blueprint $table) {
                $table->increments('id');
                $table->bigInteger('user_id')->nullable();
                $table->string('username')->nullable();
                $table->longText('message')->nullable();
                $table->longText('message_type')->nullable();
                $table->timestamps();
            });
        } else {
            Schema::table('system_logs', function (Blueprint $table) {
                if (Schema::hasColumn('system_logs', 'code')) {
                    $table->dropColumn(['code']);
                }

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_logs');
    }
}
