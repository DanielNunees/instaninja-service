<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferenceLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        if (!Schema::hasTable('reference_locations')) {
            Schema::create('reference_locations', function (Blueprint $table) {
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('user_id')->on('users');
                $table->bigInteger('external_id')->unique();
                $table->string('address');
                $table->string('name');
                $table->string('latitude');
                $table->string('longitude');
                $table->string('reference_location_user_id')->unique();
                $table->integer('total_follow')->default(0);
                $table->integer("follow_back")->default(0);
                $table->string("location_max_id", 255)->nullable(true)->default(null);
                $table->string("rank_token", 255)->nullable(true);
                $table->string("next_media_ids", 255)->nullable(true)->default(null);
                $table->boolean('is_active')->default(true);
                $table->timestamps();
            });
        } else {
            Schema::table('reference_locations', function (Blueprint $table) {
                if (!Schema::hasColumn('reference_locations', "reference_location_user_id", "total_follow", "follow_back")) {
                    $table->string('reference_location_user_id')->unique()->after("longitude");
                    $table->integer('total_follow')->default(0)->after('reference_location_user_id');
                    $table->integer("follow_back")->default(0)->after('total_follow');
                }
                if (Schema::hasColumn('reference_locations', 'total_likes')) {
                    $table->dropColumn('total_likes');
                }
                if (!Schema::hasColumn('reference_locations', 'rank_token')) {
                    $table->string("rank_token", 255)->nullable(true);
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reference_location');
    }
}
