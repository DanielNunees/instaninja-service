<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserConfigurationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        if (!Schema::hasTable('user_configuration')) {
            Schema::create('user_configuration', function (Blueprint $table) {
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('user_id')->on('users');
                $table->integer('throttled_request_time')->default(0);
                $table->integer('max_following')->default(800);
                $table->integer('follow_max')->default(12);
                $table->integer('follow_min')->default(8);
                $table->integer('follow_time_max')->default(20);
                $table->integer('follow_time_min')->default(15);
                $table->integer('unfollow_max')->default(24);
                $table->integer('unfollow_min')->default(20);
                $table->integer('unfollow_time_max')->default(30);
                $table->integer('unfollow_time_min')->default(30);
                $table->integer('like_max')->default(6);
                $table->integer('like_min')->default(4);
                $table->integer('like_time_max')->default(25);
                $table->integer('like_time_min')->default(15);
                $table->boolean('is_active')->default(true);

                $table->timestamps();
            });
        } else {
            Schema::table('user_configuration', function (Blueprint $table) {
                if (Schema::hasColumn('user_configuration', 'stop_follow_request', 'stop_like_request')) {
                    $table->dropColumn('stop_follow_request');
                    $table->dropColumn('stop_like_request');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_configuration');
    }
}
