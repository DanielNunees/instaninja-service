<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostTimelineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        if (!Schema::hasTable('post_timeline')) {
            Schema::create('post_timeline', function (Blueprint $table) {
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->bigInteger('user_id')->nullable(false);
                $table->foreign('user_id')->references('user_id')->on('users');
                $table->text('caption')->nullable();
                $table->text('comment')->nullable();
                $table->timestamp('post_at')->nullable()->default(null);
                $table->string('recurrent_post_frequency')->nullable();
                $table->string('location_lat')->nullable();
                $table->string('location_lng')->nullable();
                $table->unsignedInteger('tagged_people_id')->nullable();
                $table->boolean('already_posted')->default(false);
                $table->timestamp('posted_at')->nullable()->default(null);
                $table->timestamps();
            });
        } else {
            Schema::table('post_timeline', function (Blueprint $table) {
                if (Schema::hasColumn('post_timeline', 'people_tagged_id')) {
                    $table->dropColumn(['people_tagged_id']);
                    $table->drop('people_tagged_id');
                }

                if (!Schema::hasColumn('post_timeline', 'tagged_people_id')) {
                    $table->unsignedInteger('tagged_people_id')->nullable()->after('location_lng');
                }

                if (!Schema::hasColumn('post_timeline', 'recurrent_post_frequency')) {
                    $table->string('recurrent_post_frequency')->after("post_at")->nullable();
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_timeline');
    }
}
