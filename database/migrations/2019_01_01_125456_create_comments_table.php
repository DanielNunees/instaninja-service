<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        if (!Schema::hasTable('comments')) {
            Schema::create('comments', function (Blueprint $table) {
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->bigInteger('user_id')->nullable(false);
                $table->foreign('user_id')->references('user_id')->on('users');
                $table->text('comment')->charset('utf8mb4')->collation('utf8mb4_unicode_ci');
                $table->timestamps();
            });
        } else {
            Schema::table('comments', function (Blueprint $table) {
                if (!Schema::hasColumn('comments','user_id')) {
                    $table->bigInteger('user_id')->nullable(false);
                    $table->foreign('user_id')->references('user_id')->on('users');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
