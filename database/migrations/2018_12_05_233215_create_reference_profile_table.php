<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferenceProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        if (!Schema::hasTable('reference_profiles')) {
            Schema::create('reference_profiles', function (Blueprint $table) {
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('username')->charset('utf8mb4')->collation('utf8mb4_unicode_ci');
                $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('user_id')->on('users');
                $table->unsignedInteger('people_id');
                $table->string('reference_profile_user_id')->unique();
                $table->longText('profile_pic_url')->nullable();
                $table->integer("total_follow")->default(0);
                $table->integer("follow_back")->default(0);
                $table->string("followers_max_id", 255)->nullable(true);
                $table->string("rank_token", 255)->nullable(true);
                $table->boolean('is_active')->nullable(true);
                $table->timestamps();
            });
        } else {
            Schema::table('reference_profiles', function (Blueprint $table) {
                if (!Schema::hasColumn('reference_profiles', 'rank_token')) {
                    $table->string("rank_token", 255)->nullable(true);
                }
                if (!Schema::hasColumn('reference_profiles', 'profile_pic_url')) {
                    $table->longText('profile_pic_url')->nullable()->after('reference_profile_user_id');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reference_profile');
    }
}
