<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionsScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('actions_schedule')) {
            Schema::create('actions_schedule', function (Blueprint $table) {
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->bigInteger('user_id')->nullable(false);
                $table->foreign('user_id')->references('user_id')->on('users');
                $table->timestamp('follow_time')->nullable()->default(null);;
                $table->timestamp('unfollow_time')->nullable()->default(null);;
                $table->timestamp('like_time')->nullable()->default(null);
                $table->boolean('getting_followers')->default(false);
                $table->boolean('active');
                $table->timestamps();
            });
        } else {
            Schema::table('actions_schedule', function (Blueprint $table) {
                if (!Schema::hasColumn('actions_schedule', 'getting_followers')) {
                    $table->boolean('getting_followers')->default(false)->after('like_time');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actions_schedule');
    }
}
