<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferenceHashtagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('reference_hashtags')) {
            Schema::create('reference_hashtags', function (Blueprint $table) {
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('user_id')->on('users');
                $table->string('hashtag')->charset('utf8mb4')->collation('utf8mb4_unicode_ci');
                $table->string('reference_hashtag_user_id')->unique();
                $table->string("hashtag_max_id", 255)->nullable(true);
                $table->string("rank_token", 255)->nullable(true);
                $table->integer('total_follow')->default(0);
                $table->integer("follow_back")->default(0);
                $table->boolean('is_active')->default(true);
                $table->timestamps();
            });
        } else {
            Schema::table('reference_hashtags', function (Blueprint $table) {
                if (!Schema::hasColumn('reference_hashtags', 'follow_back')) {
                    $table->integer("follow_back")->default(0);
                }
                if (!Schema::hasColumn('reference_hashtags', 'rank_token')) {
                    $table->string("rank_token", 255)->nullable(true);
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reference_hashtag');
    }
}
