<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikesStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('likes_stats')) {
            Schema::create('likes_stats', function (Blueprint $table) {
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->engine = 'InnoDB';
                $table->increments('id');

                $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('user_id')->on('users');

                $table->string('type'); //(1)user (2)hashtag (3)location (4)feed
                $table->string('value');
                $table->integer('total_likes')->default(0);
                $table->string("likes_max_id", 255)->nullable(true);

                $table->timestamps();
            });
        } else {
            Schema::table('likes_stats', function (Blueprint $table) {
                // if (!Schema::hasColumn('users')) {
                // }

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likes_stats');
    }
}
