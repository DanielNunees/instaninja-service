<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

        if (!Schema::hasTable('post_media')) {
            Schema::create('post_media', function (Blueprint $table) {
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->engine = 'InnoDB';
                $table->increments('id');
    
                $table->unsignedBigInteger('media_id');
                $table->foreign('media_id')->references('id')->on('media');
    
                $table->integer('post_order');
    
                $table->unsignedBigInteger('post_timeline_id')->nullable();
                $table->foreign('post_timeline_id')->references('id')->on('post_timeline');
    
                $table->unsignedBigInteger('post_story_id')->nullable();
                $table->foreign('post_story_id')->references('id')->on('post_story');
    
                $table->timestamps();
            });
        } else {
            Schema::table('post_media', function (Blueprint $table) {
                // if (!Schema::hasColumn('users')) {
                // }

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_media');
    }
}
