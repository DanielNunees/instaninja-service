<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHashtagAnalyzerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('hashtag_analyzer')) {
            Schema::create('hashtag_analyzer', function (Blueprint $table) {
                $table->collation = 'utf8mb4_unicode_ci';
                $table->charset = 'utf8mb4';
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('hashtag')->charset('utf8mb4')->collation('utf8mb4_unicode_ci');
                $table->string("user_id")->nullable(false);
                $table->string('reference_profile_user_id')->nullable()->default(null);
                $table->timestamps();
            });
        } else {
            Schema::table('hashtag_analyzer', function (Blueprint $table) {
                $table->collation = 'utf8mb4_unicode_ci';
                $table->charset = 'utf8mb4';
                $table->engine = 'InnoDB';
                // if (!Schema::hasColumn('users')) {
                // }

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hashtag_analyzer');
    }
}
