<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHashtagTaggedPatternTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('hashtag_tagged_pattern')) {
            Schema::create('hashtag_tagged_pattern', function (Blueprint $table) {
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->unsignedInteger('hashtag_id');
                $table->foreign('hashtag_id')->references('id')->on('reference_hashtags');

                $table->string('pattern'); //fullathleteteam, diet, whatever
                $table->timestamps();
            });
        } else {
            Schema::table('hashtag_tagged_pattern', function (Blueprint $table) {
                // if (!Schema::hasColumn('users')) {
                // }

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hashtag_tagged_pattern');
    }
}
