<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostHourlyInsightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        if (!Schema::hasTable('post_hourly_insights')) {
            Schema::create('post_hourly_insights', function (Blueprint $table) {
                $table->increments('id');
                $table->string("user_id");
                $table->string("week_day");
                $table->string("H0");
                $table->string("H1");
                $table->string("H2");
                $table->string("H3");
                $table->string("H4");
                $table->string("H5");
                $table->string("H6");
                $table->string("H7");
                $table->string("H8");
                $table->string("H9");
                $table->string("H10");
                $table->string("H11");
                $table->string("H12");
                $table->string("H13");
                $table->string("H14");
                $table->string("H15");
                $table->string("H16");
                $table->string("H17");
                $table->string("H18");
                $table->string("H19");
                $table->string("H20");
                $table->string("H21");
                $table->string("H22");
                $table->string("H23");
                $table->timestamps();

            });
        } else {
            Schema::table('post_hourly_insights', function (Blueprint $table) {
                // if (!Schema::hasColumn('post_hourly_insights')) {
                // }

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_hourly_insights');
    }
}
