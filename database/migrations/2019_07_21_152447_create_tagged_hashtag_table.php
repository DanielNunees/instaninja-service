<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaggedHashtagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tagged_hashtag', function (Blueprint $table) {
            $table->collation = 'utf8_unicode_ci';
            $table->charset = 'utf8';
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->bigInteger('user_id')->nullable(false);
            $table->foreign('user_id')->references('user_id')->on('users');

            $table->string('hashtag')->charset('utf8mb4')->collation('utf8mb4_unicode_ci');

            $table->unsignedInteger('tagged_hashtag_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tagged_hashtag');
    }
}
