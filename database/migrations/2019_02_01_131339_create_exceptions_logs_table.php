<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExceptionsLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        if (!Schema::hasTable('exceptions_logs')) {
            Schema::create('exceptions_logs', function (Blueprint $table) {
                $table->increments('id');
                $table->bigInteger('user_id')->nullable();
                $table->string('username')->nullable();
                $table->longText('exception')->nullable();
                $table->string('exception_type')->nullable();
                $table->string('code')->nullable();
                $table->timestamps();
            });
        } else {
            Schema::table('exceptions_logs', function (Blueprint $table) {
                // if (!Schema::hasColumn('users')) {
                // }

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exceptions_logs');
    }
}
