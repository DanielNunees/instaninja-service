<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('people')) {
            Schema::create('people', function (Blueprint $table) {
                $table->collation = 'utf8mb4_unicode_ci';
                $table->charset = 'utf8mb4';
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->bigInteger('people_id');
                $table->string('username')->charset('utf8mb4')->collation('utf8mb4_unicode_ci');
                $table->string('full_name')->nullable()->charset('utf8mb4')->collation('utf8mb4_unicode_ci');
                $table->string('reference_profile_user_id')->nullable();
                $table->index('reference_profile_user_id');
                $table->string('reference_hashtag_user_id')->nullable();
                $table->index('reference_hashtag_user_id');
                $table->string('reference_location_user_id')->nullable();
                $table->index('reference_location_user_id');
                $table->boolean("friendship_status")->nullable()->default(0);
                $table->boolean('is_private')->nullable();
                $table->boolean('is_business')->nullable();
                $table->boolean('is_active')->nullable(); //means that person is or not followed yet
                $table->timestamps();
            });
        } else {
            Schema::table('people', function (Blueprint $table) {
                if (!Schema::hasColumn('people', "reference_hashtag_user_id", "reference_location_user_id")) {
                    $table->string('reference_hashtag_user_id')->nullable();
                    $table->index('reference_hashtag_user_id');
                    $table->string('reference_location_user_id')->nullable();
                    $table->index('reference_location_user_id');
                }

                if (Schema::hasColumn('people', "followers_count", "gender", "following_count", "following_tag_count", "media_count", "email", "profile_pic_url")) {
                    $table->dropColumn('followers_count');
                    $table->dropColumn('gender');
                    $table->dropColumn('following_count');
                    $table->dropColumn('following_tag_count');
                    $table->dropColumn('media_count');
                    $table->dropColumn('email');
                    $table->dropColumn('profile_pic_url');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
