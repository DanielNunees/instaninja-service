<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaggedPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tagged_people')) {
            Schema::create('tagged_people', function (Blueprint $table) {
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->engine = 'InnoDB';
                $table->increments('id');

                $table->bigInteger('user_id')->nullable(false);
                $table->foreign('user_id')->references('user_id')->on('users');

                $table->string('username')->charset('utf8mb4')->collation('utf8mb4_unicode_ci');
                $table->unsignedBigInteger('people_id');

                $table->unsignedBigInteger('tagged_people_id');
                $table->timestamps();
            });
        } else {
            Schema::table('tagged_people', function (Blueprint $table) { });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tagged_people');
    }
}
