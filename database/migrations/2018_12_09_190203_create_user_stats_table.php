<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('user_stats')) {
            Schema::create('user_stats', function (Blueprint $table) {
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->unsignedBigInteger('user_id');
                $table->foreign('user_id')->references('user_id')->on('users');

                $table->integer('followers_count')->default(0);
                $table->integer('following_count')->default(0);
                $table->integer('media_count')->default(0);
                $table->integer('total_followed')->default(0);
                $table->integer('total_following_back')->default(0);
                $table->integer('total_user_media_likes')->default(0);
                $table->integer('total_hashtag_media_likes')->default(0);
                $table->integer('total_location_media_likes')->default(0);
                $table->timestamps();
            });
        } else {
            Schema::table('user_stats', function (Blueprint $table) {
                // if (!Schema::hasColumn('users')) {
                // }

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_stats');
    }
}
