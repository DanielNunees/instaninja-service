<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('media')) {
            Schema::create('media', function (Blueprint $table) {
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->bigInteger()('user_id');
                $table->foreign('user_id')->references('user_id')->on('users');
                $table->string('file_name');
                $table->string('folder_name');
                $table->string('media_type');
                $table->string('url');
                $table->string('path');

                $table->timestamps();
            });
        } else {
            Schema::table('media', function (Blueprint $table) {
                if (!Schema::hasColumn('media', 'user_id')) {
                    $table->unsignedBigInteger('user_id')->after('id');
                    $table->foreign('user_id')->references('user_id')->on('users');
                }
                if (Schema::hasColumn('media', 'instagram_media_id')) {
                    $table->drop('instagram_media_id');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
