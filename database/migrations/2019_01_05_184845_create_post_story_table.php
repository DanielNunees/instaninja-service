<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostStoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        if (!Schema::hasTable('post_story')) {
            Schema::create('post_story', function (Blueprint $table) {
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->bigInteger('user_id')->nullable(false);
                $table->foreign('user_id')->references('user_id')->on('users');

                $table->unsignedBigInteger('tagged_people_id')->nullable();
                $table->unsignedBigInteger('tagged_hashtag_id')->nullable();
                $table->string('caption')->nullable();

                $table->string('recurrent_post_frequency');

                $table->string('location_lat')->nullable();
                $table->string('location_lng')->nullable();

                $table->timestamp('post_at')->nullable()->default(null);
                $table->boolean('already_posted')->default(false);

                $table->timestamp('posted_at')->nullable()->default(null);
                $table->timestamps();
            });
        } else {
            Schema::table('post_story', function (Blueprint $table) {
                if (Schema::hasColumn('post_story', 'hashtag_pattern')) {
                    $table->drop('hashtag_pattern')->nullable();
                }
                if (Schema::hasColumn('post_story', 'people_tagged_id')) {
                    $table->dropForeign(['people_tagged_id']);
                    $table->drop('people_tagged_id');
                }
                if (!Schema::hasColumn('post_story', 'tagged_people_id')) {
                    $table->unsignedBigInteger('tagged_people_id')->nullable()->after('user_id');
                }

                if (!Schema::hasColumn('post_story', 'tagged_hashtag_id')) {
                    $table->unsignedBigInteger('tagged_hashtag_id')->nullable()->after('tagged_people_id');
                }

                if (!Schema::hasColumn('post_story', 'caption')) {
                    $table->string('caption')->nullable()->after('tagged_hashtag_id');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_story');
    }
}
